"use strict";

const Router = require("koa-router");
const session = require("koa-session");
const author = require("../lib/authorize");
const sessionConfig = require("../lib/session");

module.exports = app => {
  const auth = require("../controllers/authController");
  const file = require("../controllers/fileController");
  const user = require("../controllers/userController");
  const tweet = require("../controllers/tweetController");
  const notification = require("../controllers/notificationController");
  const message = require("../controllers/messageController");

  app.keys = ["supersecret"];

  const router = new Router()
    .use(session(sessionConfig.config, app))
    .use(author.authorize)
    //Auth
    .post("/auth/signup", auth.signup)
    .post("/auth/signin", auth.signin)
    .get("/auth/signout", auth.signout)
    .post("/auth/verify", auth.verify)
    //Upload
    .post("/upload", file.upload)
    //User
    .patch("/user/:id", user.update)
    .put("/user/:id/follow", user.follow)
    .delete("/user/:id/follow", user.unfollow)
    .get("/user/:id/follow", user.getfollow)
    .get("/user/:id/followed", user.getfollowed)
    //Tweet
    .get("/tweet", tweet.gettweet)
    .post("/tweet", tweet.tweet)
    .put("/tweet/:id/like", tweet.like)
    .delete("/tweet/:id/like", tweet.unlike)
    .post("/tweet/:id/retweet", tweet.retweet)
    .put("/tweet/:id/vote/:voteId", tweet.vote)
    .post("/tweet/:id/reply", tweet.reply)
    // //Notification
    .get("/notification", notification.notification)
    // //Direct Message
    .get("/message", message.listmessage)
    .get("/message/:userId", message.getmessage)
    .post("/message/:userId", message.sendmessage);

  app.use(router.allowedMethods());
  return router.routes();
};
