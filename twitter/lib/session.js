"use strict"

const pool = require('../lib/pool');
let _ = require('lodash');
const sessionStore = {}

exports.config = {
    key: 'sess',
    maxAge: 3600 * 1000,
    httpOnly: true,
    store: {
        async get (key, maxAge, { rolling }) {  
            const db = await pool.getConnection()   
            const [rows] = await db.execute(`select id, value from sessions where id = ?`, [key])
            db.release()
            if (!_.isEmpty(rows)) {
                return JSON.parse(rows[0].value)
            }
        },
        async set (key, sess, maxAge, { rolling }) {     
            const db = await pool.getConnection()   
            let sess_data = JSON.stringify(sess)
            await db.execute(`insert into sessions (id, value) values (?, ?) on duplicate key update value = ?`, [key, sess_data, sess_data])
            db.release()
            sessionStore[key] = sess
        },
        destroy (key) {
            delete sessionStore[key]
        }
    }
}