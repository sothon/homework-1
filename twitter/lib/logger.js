"use strict"

exports.logger = function (ctx) {
    const start = process.hrtime()
    const diff = process.hrtime(start)
    console.log(`${ctx.method} ${ctx.path} ${diff[0] * 1e9 + diff[1]}ns`)
}