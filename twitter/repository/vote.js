"use strict"

module.exports = {
    create
}
  
async function create (db, userId, tweetId, choiceId) {
const result = await db.execute(`
    insert into votes (user_id, tweet_id, choice_id) values (?, ?, ?)
    `, [
        userId, tweetId, choiceId
    ]
)
return result[0].insertId
}