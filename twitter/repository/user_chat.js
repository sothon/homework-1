"use strict"

module.exports = {
    create,
    getMessage,
    getWithReceiver,
    markRead
}
  
async function create (db, userChat) {
    const result = await db.execute(`
        insert into user_chats (sender_id, receiver_id, content, type, is_read) values (?, ?, ?, ?, ?)
        `, [
            userChat.sender_id, userChat.receiver_id, userChat.content, userChat.type, 0
        ]
    )
    return result[0].insertId
}

async function getMessage (db, sender_id) {
    const [rows] = await db.execute(`
        select id, receiver_id, content, type, is_read, created_at from user_chats where sender_id = ? 
        `, [
            sender_id
        ]
    )
    return rows
}

async function getWithReceiver (db, sender_id, receiver_id) {
    const [rows] = await db.execute(`
        select id, receiver_id, content, type, is_read, created_at from user_chats where sender_id = ? and receiver_id = ?
        `, [
            sender_id, receiver_id
        ]
    )
    return rows
}

async function markRead (db, userChat_id) {
    const [rows] = await db.execute(`
        update user_chats set type = ? where id = ? 
        `, [
            1 ,userChat_id
        ]
    )
    return rows[0]
}
