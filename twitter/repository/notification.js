"use strict"

module.exports = {
    create,
    getnotification,
    markAsRead
  }
  
  async function create (db, userId, noti) {
    const result = await db.execute(
      `insert into notifications (user_id, title, content, photo, is_read) values (?, ?, ?, ?, ?)`, 
      [userId, noti.title, noti.content, noti.photo, 0]
    )
    return result[0].insertId
  }
  
  async function getnotification (db, user_id) {
    const [rows] = await db.execute(`
        select id, user_id, content as text, photo, is_read, created_at as createdAt from notifications where user_id = ? and is_read = ?
        `, [
            user_id, 0
        ]
    )
    return rows
  }

  async function markAsRead (db, notiId) {
    await db.execute(
      `update notifications set is_read = 1 where id = ?`, 
      [notiId]
    )
  }