"use strict"

module.exports = {
    create,
    addChoice,
    addVote
}
  
async function create (db, tweetId, poll) {
  const result = await db.execute(`
    insert into tweet_polls (tweet_id, end_time) values (?, ?)
    `, [
      tweetId, poll.end
    ]
  )
  return result[0].insertId
}
  
async function addChoice (db, tweetId, content) {
  const result = await db.execute(`
    insert into tweet_choices (tweet_id, content) values (?, ?)
    `, [
      tweetId, content
    ]
  )
  return result[0].insertId
}
  
async function addVote (db, userId, tweetId, choiceId) {
  const result = await db.execute(`
    insert into tweet_choices (user_id, tweet_id, choice_id) values (?, ?, ?)
    `, [
      userId, tweetId, choiceId
    ]
  )
  return result[0].insertId
}