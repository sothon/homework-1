"use strict"

const _ = require('lodash');

module.exports = {
    create,
    gettweet,
    read,
    update,
    remove,
    addPhoto,
    like,
    unlike,
    tweet_hashtags,
    retweet,
    reply
}
  
async function create (db, userId, tweet) {
  const result = await db.execute(`
    insert into tweets (user_id, content, type) values (?, ?, ?)
    `, [
      userId, tweet.text, tweet.type
    ]
  )
  return result[0].insertId
}

async function gettweet (db, userId) {
  const [rows] = await db.execute(`
    select id, user_id, content, type, created_at from tweets where user_id = ? 
    `, [
      userId
    ]
  )
  return rows
}

async function read (db, tweetId) {
  const [rows] = await db.execute(`
    select id, user_id, content, type, created_at from tweets where id = ? 
    `, [
      tweetId
    ]
  )
  return rows[0]
}

async function update (db, tweet) {
  await db.execute(`
    update tweets set content = ? where id = ? 
    `, [
      tweet.content, tweet.id
    ]
  )
}

async function remove (db, tweetId) {
  await db.execute(`
    delete from tweets where id = ? 
    `, [
      tweetId
    ]
  )
}

async function addPhoto (db, tweetId, photoUrl) {
  const result = await db.execute(`
    insert into tweet_photos (tweet_id, url) values (?, ?)
    `, [
      tweetId, photoUrl
    ]
  )
  return result[0].insertId
}

async function like (db, userId, tweetId) {
  const [rows] = await db.execute(`
    select user_id, tweet_id from tweet_likes where user_id = ? and tweet_id = ?
    `, [
      userId, tweetId
    ]
  )
  if (_.isEmpty(rows)) {
    await db.execute(`
      insert into tweet_likes (user_id, tweet_id) values (?, ?)  
      `, [
        userId, tweetId
      ]
    )
  }
}

async function unlike (db, userId, tweetId) {
  await db.execute(`
    delete from tweet_likes where user_id = ? and tweet_id = ?
    `, [
      userId, tweetId
    ]
  )
}

async function tweet_hashtags (db, tweetId, hashtag) {
  await db.execute(`
    insert into tweet_hashtags (tweetId, hashtag) values (?, ?)
    `, [
      tweetId, hashtag
    ]
  )
}

async function retweet (db, userId, tweetId, content) {
  await db.execute(`
    insert into retweets (user_id, tweet_id, content) values (?, ?, ?)
    `, [
      userId, tweetId, content
    ]
  )
}

async function reply (db, userId, tweetId, content) {
  await db.execute(`
    insert into tweet_replies (user_id, tweet_id, content) values (?, ?, ?)
    `, [
      userId, tweetId, content
    ]
  )
}

