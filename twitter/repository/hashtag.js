"use strict"


module.exports = {
    create
  }

async function hashtag (db, name) {
    await db.execute(`
      insert into hashtags (name) values (?)
      `, [
        name
      ]
    )
  }