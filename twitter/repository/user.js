"use strict"

module.exports = {
    create,
    read,
    update,
    remove,
    getbyEmail,
    changePhoto,
    changeCover,
    follow,
    unfollow,
    getfollow,
    getfollowed,
    chat
}

async function create (db, user) {
  if (!user.id) {
    const result = await db.execute(`
      insert into users (username, email, password, name, status) values (?, ?, ?, ?, ?)
      `, [
      user.username, user.email, user.password, user.name, 0
      ]
    )
    return result.insertId
  }
  return update(db, user)
}

async function read (db, userId) {
  const [rows] = await db.execute(`
    select id, username, email, password, name, status, photo, cover, location, bio, theme,
    birth_date_d, birth_date_m, birth_date_y, show_dm, show_y
    from users where id = ? 
    `, [
      userId
    ]
  )
  return rows[0]
}

async function update (db, user) {
  await db.execute(`
    update users set name = ?, photo = ?, cover = ?, location = ?, bio = ?, theme = ?, 
    birth_date_d = ?, birth_date_m = ?, birth_date_y = ?, show_dm = ?, show_y = ? where id = ? 
    `, [
      user.name, user.profilePhoto, user.coverPhoto, user.location, user.bio, user.theme,
      user.birthDateD, user.birthDateM, user.birthDateY, user.showBirthDateY, user.showBirthDateDM, 
      user.id
    ]
  )
}

async function remove (db, user) {
  await db.execute(`
    update users set status = ? where id = ? 
    `, [
      user.status, user.id
    ]
  )
}

async function getbyEmail (db, email) {
  // console.log(username) 
  const [rows] = await db.execute(`
    select id, username, email, password, name, status from users where email = ? 
    `, [
      email
    ]
  )
  // console.log(rows)
  return rows[0]
}

async function changePhoto (db, userId, photoUrl) {
  await db.execute(`
    update users set photo = ? where id = ?
    `, [
      photoUrl, userId
    ]
  )
}

async function changeCover (db, userId, photoUrl) {
  await db.execute(`
    update users set cover = ? where id = ?
    `, [
      photoUrl, userId
    ]
  )
}

async function follow (db, followerId, followingId) {
  const [rows] = await db.execute(`
    select follower_id, following_id from follows where follower_id = ? and following_id = ?
    `, [
      followerId, followingId
    ]
  )
  if (rows[0] === undefined) {
    const result = await db.execute(`
      insert into follows (follower_id, following_id) values (?, ?)
      `, [
        followerId, followingId
      ]
    )
    return result[0].insertId
  }
}

async function unfollow (db, followerId, followingId) {
  await db.execute(`
    delete from follows where follower_id = ? and following_id = ?
    `, [
      followerId, followingId
    ]
  )
}

async function getfollow (db, follower_id) {
  const [rows] = await db.execute(`
    select id, username, photo as profilePhoto, cover as coverPhoto, false as isFollow
    from users
      left join follows on follows.following_id = users.id
    where follows.follower_id = ? 
    `, [
      follower_id
    ]
  )
  return rows
}

async function getfollowed (db, following_id) {
  const [rows] = await db.execute(`
    select id, username, photo as profilePhoto, cover as coverPhoto, false as isFollow
    from users
      left join follows on follows.follower_id = users.id
    where follows.following_id = ? 
    `, [
      following_id
    ]
  )
  return rows
}

async function chat (db, userChat) {
  const result = await db.execute(`
    insert into user_chat (sender_id, receiver_id, content, type) values (?, ?, ?, ?)
    `, [
      userChat.sender_id, userChat.receiver_id, userChat.content, userChat.type
    ]
  )
  return result[0].insertId
}

