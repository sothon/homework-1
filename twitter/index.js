"use strict"

const Koa = require('koa') 
const bodyParser = require('koa-bodyparser')
const requestLogger = require('./lib/logger')

const app = new Koa()

app.use(bodyParser())
const router = require('./routes/twitterRoute')(app)  
app.use(router)
app.use(requestLogger.logger);
app.listen(3000)
