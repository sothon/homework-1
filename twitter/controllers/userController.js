"use strict";

const userRepo = require("../repository/user");
const notificationRepo = require("../repository/notification");
const pool = require("../lib/pool");
const _ = require("lodash");

exports.update = async function(ctx, next, app) {
  const db = await pool.getConnection();
  let result = {};
  try {
    // input = {
    //     "name": "Tester Kirito",
    //     "profilePhoto": "https://example.com/image1",
    //     "coverPhoto": "https://example.com/image2",
    //     "location": "Bangkok, Thailand",
    //     "bio": "Inw True Kirito",
    //     "theme": "#00ff00",
    //     "birthDateD": "12",
    //     "birthDateM": "09",
    //     "birthDateY": null,
    //     "showBirthDateY": false,
    //     "showBirthDateDM": true
    // }
    // console.log(ctx.request.body)
    let updateUser = JSON.parse(JSON.stringify(ctx.request.body));
    updateUser.id = _.isEmpty(ctx.params.id) ? "" : ctx.params.id;
    console.log("updateUser.id=>", updateUser.id);
    console.log("ctx.session.user.id=>", ctx.session.user.id);
    if (updateUser.id !== "") {
      if (parseInt(updateUser.id) === parseInt(ctx.session.user.id)) {
        await userRepo.update(db, updateUser);
        result = {
          status: "success"
        };
      } else {
        result = {
          error: "unauthorized"
        };
      }
    }
    console.log(result);
    ctx.body = result;
    await next();
  } catch (err) {
    ctx.status = 400;
    ctx.body = err.message;
  } finally {
    db.release();
  }
};

exports.follow = async function(ctx, next) {
  const db = await pool.getConnection();
  let result = {};
  try {
    let followingId = _.isEmpty(ctx.params.id) ? "" : ctx.params.id;
    if (followingId !== "") {
      let existUser = await userRepo.read(db, followingId);
      if (!_.isEmpty(existUser)) {
        let followerId = ctx.session.user.id;
        await userRepo.follow(db, followerId, followingId);
        let noti = {
          title: "followed",
          content: "followed by " + followerId,
          photo: ""
        };
        await notificationRepo.create(db, followingId, noti);
        result = {
          status: "success"
        };
      } else {
        result = {
          error: "invalid user following"
        };
      }
    }
    ctx.body = result;
    await next();
  } catch (err) {
    ctx.status = 400;
    ctx.body = err.message;
  } finally {
    db.release();
  }
};

exports.unfollow = async function(ctx, next) {
  const db = await pool.getConnection();
  let result = {};
  try {
    let followingId = _.isEmpty(ctx.params.id) ? "" : ctx.params.id;
    if (followingId !== "") {
      let existUser = await userRepo.read(db, followingId);
      if (!_.isEmpty(existUser)) {
        let followerId = ctx.session.user.id;
        await userRepo.unfollow(db, followerId, followingId);
        result = {
          status: "success"
        };
      } else {
        result = {
          error: "don't followed user"
        };
      }
    }
    ctx.body = result;
    await next();
  } catch (err) {
    ctx.status = 400;
    ctx.body = err.message;
  } finally {
    db.release();
  }
};

exports.getfollow = async function(ctx, next) {
  const db = await pool.getConnection();
  let result = {};
  try {
    let followerId = _.isEmpty(ctx.params.id) ? "" : ctx.params.id;
    if (followerId !== "") {
      let existUser = await userRepo.read(db, followerId);
      if (!_.isEmpty(existUser)) {
        let followlist = await userRepo.getfollow(db, followerId);
        if (followlist.length > 0) {
          result = followlist;
        } else {
          result = {
            status: "success",
            info: "no row return"
          };
        }
      } else {
        result = {
          error: "invalid user"
        };
      }
    }
    ctx.body = result;
    await next();
  } catch (err) {
    ctx.status = 400;
    ctx.body = err.message;
  } finally {
    db.release();
  }
};

exports.getfollowed = async function(ctx, next) {
  const db = await pool.getConnection();
  let result = {};
  try {
    let followingId = _.isEmpty(ctx.params.id) ? "" : ctx.params.id;
    if (followingId !== "") {
      let existUser = await userRepo.read(db, followingId);
      if (!_.isEmpty(existUser)) {
        let followedlist = await userRepo.getfollowed(db, followingId);
        if (followedlist.length > 0) {
          result = followedlist;
        } else {
          result = {
            status: "success",
            info: "no row return"
          };
        }
      } else {
        result = {
          error: "invalid user"
        };
      }
    }
    ctx.body = result;
    await next();
  } catch (err) {
    ctx.status = 400;
    ctx.body = err.message;
  } finally {
    db.release();
  }
};
