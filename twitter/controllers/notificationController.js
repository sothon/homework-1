"use strict"

const notificationRepo = require('../repository/notification')
const pool = require('../lib/pool');
const _ = require('lodash');

exports.notification = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    try {
        // input =  {
        //     "notifications": [
        //         {
        //             "id": 46,
        //             "text": "Player follow you.",
        //             "photo": "...",
        //             "read": false,
        //             "createdAt": "2018-01-14T09:58:15.551Z"
        //         }
        //     ]
        // }
        if (!_.isEmpty(ctx.session.user)) {
            let notifications = await notificationRepo.getnotification(db, ctx.session.user.id)
            if (notifications.length > 0) {           
                result.notifications = notifications                
            } else {
                result = {
                    "status": "success",
                    "info":"no row return"
                }
            }      
        } 
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
}