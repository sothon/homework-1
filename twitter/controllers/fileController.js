"use strict"

const pool = require('../lib/pool');
const multer = require('koa-multer')
const sharp = require('sharp')
const fs = require('fs')

exports.upload = async function (ctx, next)  {
    let result = {"status": "success"}
    try {
        // ignore non-POSTs
        if ('POST' != ctx.method) return await next();

        const upload = multer({ dest: 'upload/' })
        await upload.single('file')(ctx)
        const tempFile = ctx.req.file.path
        const outFile = tempFile + '.png'
        await sharp(tempFile)
            .resize(100, 100)
            .toFile(outFile)
    
        fs.unlink(tempFile, () => {})
        result = {
            "upload": "success",
            "file": outFile
        }
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    }
}