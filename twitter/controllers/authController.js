"use strict"

const userRepo = require('../repository/user')
const pool = require('../lib/pool');
const _ = require('lodash');
const bcrypt = require('bcrypt-promise')


exports.signup = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    try {
        // input = {
        //     "email": "test@example.com",
        //     "username": "tester",
        //     "password": "password 1234 lol ezez!!!!",
        //     "name": "Tester Hajime"
        // }        
        
        let newUser = ctx.request.body
        let { email, username, password, name } = newUser
        if (email && username && password && name) {    
            let existUser = await userRepo.getbyEmail(db, newUser.email)
            if (_.isEmpty(existUser)) {
                newUser.password = await bcrypt.hash(newUser.password, 10)
                await userRepo.create(db, newUser)    
                result = {
                    "status": "success"
                }  
            } else {
                result = {
                    "error": "duplicate email"
                }
            }
        } else {
            result = {
                "error": "invalid input"
            }
        }       
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
}

exports.signin = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    try {
        // input = {
        //     "email": "test@example.com",
        //     "password": "password 1234 lol ezez!!!!"
        // }
        let { email, password } = ctx.request.body
        if (email && password) {
            let existUser = await userRepo.getbyEmail(db, email)
            if (!_.isEmpty(existUser)) {
                let hashed = existUser.password
                let inputpassword = await bcrypt.hash(password, 10)
                let same = await bcrypt.compare(password, hashed)
                if(!same) {
                    result = {
                        "error": "wrong password"
                    }  
                } else {
                    ctx.session.user = existUser
                    result = {
                        "status": "success"
                    }  
                }
            } else {
                result = {
                    "error": "email is invalid"
                }           
            }
        } else {
            result = {
                "error": "invalid input"
            }
        }
        
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
}

module.exports.signout = async function (ctx, next)  {
    let result = {}
    try {    
        ctx.session.user = {}    
        result = {
            "status": "success"
        }  
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    }
}

exports.verify = async function (ctx, next)  {
    let result = {
        "status": "success"
    }
    try {
        // input = {
        //    "token": "123456789"
        // }        
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    }
}