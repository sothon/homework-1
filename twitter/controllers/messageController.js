"use strict"

const chatRepo = require('../repository/user_chat')
const userRepo = require('../repository/user')
const notificationRepo = require('../repository/notification')
const pool = require('../lib/pool');
const _ = require('lodash');

exports.sendmessage = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    try {
        // input = {
        //     "text": "Hello"
        // }            
        if (!_.isEmpty(ctx.params.userId)) {
            let userChat = {}
            userChat.sender_id = ctx.session.user.id
            userChat.receiver_id = ctx.params.userId
            userChat.content = _.isUndefined(ctx.request.body.text) ? '' : ctx.request.body.text    
            userChat.type = 0    
            if (userChat.receiver_id !== '') {
                let existUser = await userRepo.read(db, userChat.receiver_id)
                if (!_.isEmpty(existUser)) {
                    await chatRepo.create(db, userChat)  
                    let noti = {
                        "title": "direct message",
                        "content": "message from " + userChat.sender_id,
                        "photo": ""
                    }
                    await notificationRepo.create(db, userChat.receiver_id, noti)                       
                    result = {
                        "status": "success"
                    }  
                } else {
                    result = {
                        "error": "invalid receiver_id"
                    }
                }        
            } else {
                result = {
                    "error": "invalid receiver_id"
                }
            }
        }         
        ctx.body = result    
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
}

exports.getmessage = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    try {
        // input = {
        //     "user": {
        //         "id": 4,
        //         "username": "Player",
        //         "profilePhoto": "...",
        //         "coverPhoto": "...",
        //         "bio": "...",
        //         "isFollow": true
        //     },
        //     "messages": [
        //         {
        //             "text": "Hi",
        //             "createdAt": "2018-01-14T09:58:15.551Z",
        //             "read": true
        //         }
        //     ]
        // }        
        if (!_.isEmpty(ctx.params.userId)) {
            let userChat = {}
            userChat.sender_id = ctx.session.user.id
            userChat.receiver_id = ctx.params.userId
            if (userChat.receiver_id !== '') {
                let existUser = await userRepo.read(db, userChat.receiver_id)
                if (!_.isEmpty(existUser)) {                          
                    let user_detail = {}
                    user_detail.id = existUser.id
                    user_detail.username = existUser.username
                    user_detail.profilePhoto = existUser.photo
                    user_detail.coverPhoto = existUser.cover
                    user_detail.bio = existUser.bio
                    user_detail.isFollow = 0
                    result.user = user_detail  
                    let chatlist = await chatRepo.getWithReceiver(db, userChat.sender_id, userChat.receiver_id)
                    if (chatlist.length > 0) {
                        result.user.message = chatlist    
                    } else {
                        result = {
                            "status": "success",
                            "info":"no row return"
                        }
                    }                
                } else {
                    result = {
                        "error": "invalid receiver_id"
                    }
                }      
            } else {
                result = {
                    "error": "invalid receiver_id"
                }
            }
        } 
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
}


exports.listmessage = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    try {
        // input = {
        //     "messages": [
        //         {
        //             "text": "Hi",
        //             "createdAt": "2018-01-14T09:58:15.551Z",
        //             "read": false,
        //             "user": {
        //                 "id": 4,
        //                 "username": "Player",
        //                 "profilePhoto": "...",
        //                 "coverPhoto": "...",
        //                 "bio": "...",
        //                 "isFollow": true
        //             }
        //         }
        //     ]
        // }
        let sender_id = ctx.session.user.id
        if (!_.isUndefined(sender_id)) {
            let listChat = await chatRepo.getMessage(db, sender_id)
            if (listChat.length > 0) {
                result.message = listChat
                result.message.map(async (obj) => {
                    let receiver_user = await userRepo.read(db, obj.receiver_id)
                    let delail_user = {}            
                    delail_user.id = receiver_user.receiver_id
                    delail_user.username = receiver_user.username
                    delail_user.profilePhoto = receiver_user.photo
                    delail_user.coverPhoto = receiver_user.cover
                    delail_user.bio = receiver_user.bio
                    delail_user.isFollow = false
                    obj.user = delail_user
                })
            } else {
                result = {
                    "status": "success",
                    "info":"no row return"
                }
            }
        }
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
}

