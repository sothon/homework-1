"use strict"

const pool = require('../lib/pool');
const _ = require('lodash');
const tweetRepo = require('../repository/tweet')
const pollRepo = require('../repository/poll')
const voteRepo = require('../repository/vote')
const notificationRepo = require('../repository/notification')

exports.gettweet = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    try {
        // input = {
        // }
        let userId = ctx.session.user.id
        if (userId) {
            console.log(userId)
            let tweetlist = await tweetRepo.gettweet(db, userId)
            if (tweetlist.length > 0) {
                result = tweetlist
            } else {
                result = {
                    "status": "success",
                    "info":"no row return"
                }
            }
        } else {
            result = {
                "error": "invalid user"
            }
        }
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    }
}

exports.tweet = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    // try {
        // input = {
        //     "text": "best text editor in 2018 ?",
        //     "photos": [
        //         "https://example.com/photo1"
        //     ],
        //     "poll": {
        //         "end": "2018-01-16T10:51:12.124Z",
        //         "votes": [
        //             "VS Code",
        //             "Atom",
        //             "Vim"
        //         ]
        //     }
        // }
        let userId = ctx.session.user.id
        let newTweet = ctx.request.body
        let { text, photos, poll } = newTweet        
        if (poll) {
            newTweet.type = 2
            if (userId && text) {
                newTweet.id = await tweetRepo.create(db, userId, newTweet) 
                if (newTweet.id && poll) {
                    let newPoll = poll
                    if (newPoll.end) {
                        newPoll.id = await pollRepo.create(db, newTweet.id, newPoll)
                        if (newTweet.id && newPoll.votes) {
                            newPoll.votes.map(async (vote) => {
                                await pollRepo.addChoice(db, newTweet.id, vote)                                                            
                            });    
                            result = {
                                "status": "success"
                            } 
                        } else {
                            result = {
                                "error": "invalid poll"
                            } 
                        }                        
                    } else {
                        result = {
                            "error": "invalid input"
                        } 
                    }                                       
                } else {
                    result = {
                        "error": "invalid input"
                    } 
                }
            }  else {
                result = {
                    "error": "invalid input"
                } 
            }          
        } else {
            newTweet.type = 1
            if (userId && text) {
                newTweet.id = await tweetRepo.create(db, userId, newTweet) 
                result = {
                    "status": "success"
                } 
            } else {
                result = {
                    "error": "invalid input"
                } 
            }
        }
        ctx.body = result
        await next();
    // } catch (err) {
    //     ctx.status = 400
    //     ctx.body = err.message
    // } finally {
        db.release()
    // }
}

exports.like = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    try {
        if (ctx.params.id) {
            let tweetId = ctx.params.id
            let existTweet = await tweetRepo.read(db, tweetId)
            if (!_.isEmpty(existTweet)) {
                let userId = ctx.session.user.id
                await tweetRepo.like (db, userId, tweetId)   
                let noti = {
                    "title": "liked",
                    "content": "liked by " + userId,
                    "photo": ""
                }
                await notificationRepo.create(db, existTweet.user_id, noti)              
                result = {
                    "status": "success"
                } 
            }
        } else {
            result = {
                "error": "invalid tweet id"
            }
        }
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
}

exports.unlike = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    try {
        if (ctx.params.id) {
            let tweetId = ctx.params.id
            let existTweet = await tweetRepo.read(db, tweetId)
            if (!_.isEmpty(existTweet)) {
                let userId = ctx.session.user.id
                await tweetRepo.unlike (db, userId, tweetId) 
                result = {
                    "status": "success"
                } 
            }
        } else {
            result = {
                "error": "invalid tweet id"
            }
        }
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
}

exports.retweet = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    try {
        if (ctx.params.id) {
            let tweetId = ctx.params.id
            let existTweet = await tweetRepo.read(db, tweetId)
            if (!_.isEmpty(existTweet)) {
                let userId = ctx.session.user.id
                await tweetRepo.retweet (db, userId, tweetId, existTweet.content)      
                let noti = {
                    "title": "retweeted",
                    "content": "retweeted by " + userId,
                    "photo": ""
                }
                await notificationRepo.create(db, existTweet.user_id, noti)               
                result = {
                    "status": "success"
                } 
            }
        } else {
            result = {
                "error": "invalid tweet id"
            }
        }
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
}

exports.vote = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    try {
        if (ctx.params.id && ctx.params.voteId) {
            let tweetId = ctx.params.id
            let voteId = ctx.params.voteId
            let existTweet = await tweetRepo.read(db, tweetId)
            if (!_.isEmpty(existTweet)) {
                let userId = ctx.session.user.id
                await voteRepo.create (db, userId, tweetId, voteId) 
                let noti = {
                    "title": "voted",
                    "content": "voted by " + userId,
                    "photo": ""
                }
                await notificationRepo.create(db, existTweet.user_id, noti)                    
                result = {
                    "status": "success"
                } 
            }
        } else {
            result = {
                "error": "invalid tweet id"
            }
        }
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
}

exports.reply = async function (ctx, next)  {
    const db = await pool.getConnection()
    let result = {}
    try {
        if (ctx.params.id) {
            let tweetId = ctx.params.id
            let existTweet = await tweetRepo.read(db, tweetId)
            if (!_.isEmpty(existTweet)) {
                let userId = ctx.session.user.id
                await tweetRepo.reply (db, userId, tweetId, existTweet.content) 
                let noti = {
                    "title": "replyed",
                    "content": "replyed by " + userId,
                    "photo": ""
                }
                await notificationRepo.create(db, existTweet.user_id, noti)                    
                result = {
                    "status": "success"
                } 
            }
        } else {
            result = {
                "error": "invalid tweet id"
            }
        }
        ctx.body = result
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
}