
insert into employees (name, surname,age) 
values 
    ('Luke','Skywalker',25),
    ('Tony','Stark',40),
    ('Cinderella','Disney',18),
    ('Spider','Man',22),
    ('Iron','Man',40),
    ('Captain','America',35),
    ('Ant','Man',28),
    ('Iceman','X-Men',20),
    ('Beast','X-Men',49),
    ('Wolverine','X-Men',32);


delete from employees where emp_id = 5;

alter table employees add column address varchar(200);

update employees set address = 'address for test' where emp_id in (1,3,7,9);

select count(*) from employees;

select name,surname from employees where age < 20;

