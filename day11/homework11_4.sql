show databases;
use employees;
show tables;

describe current_dept_emp;
describe departments;
describe dept_emp;
describe dept_emp_latest_date;
describe dept_manager;
describe employees;
describe salaries;
describe titles;

select distinct title from titles;

select gender, count(gender) cnt from employees group by gender;

select count(distinct last_name) as cnt_lastname from employees;

