
create table employees (
    emp_id      int auto_increment,
    name        varchar(100),
    surname     varchar(100),
    age         int,
    created_at  timestamp default now(),
    primary key (emp_id)
);

create table books (
    isbn        varchar(50) not null,
    book_name   varchar(100),
    price       decimal,
    created_at timestamp default now(),
    primary key (isbn)
);

create table orders (
    order_id    int auto_increment,
    isbn        varchar(50) not null,
    emp_id      int,
    price       decimal,
    quantity    int,
    created_at  timestamp default now(),
    primary key (order_id)
);
