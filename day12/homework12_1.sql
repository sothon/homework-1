--มใครบางทไมไดสอนอะไรเลย
select 
    instructors.id, instructors.name as instructor_name, courses.name as course_name
from instructors
    left join courses on instructors.id = courses.teach_by
where courses.name is null
order by instructors.id;

--มคอรสไหนบาง ทไมมคนสอน
select 
    courses.id, courses.name as course_name, instructors.name as instructor_name
from courses
    left join instructors on instructors.id = courses.teach_by
where instructors.name is null
order by courses.id;





