--หาวาพนกงานแตละคน มใครเปน manager บาง
select 
    employees.*    
from     
    employees
    inner join dept_manager on dept_manager.emp_no = employees.emp_no;

--หาวาพนกงานคนไหน เงนเดอนมากกวา manager บาง
select 
    employees.*, salaries.salary, dept_emp.dept_no, dept_manager.emp_no as manager_id, 
    salaries_manager.salary as salaries_manager
from     
    employees
    inner join salaries on salaries.emp_no = employees.emp_no
    inner join dept_emp on dept_emp.emp_no = employees.emp_no
    inner join dept_manager on dept_manager.dept_no = dept_emp.dept_no
    inner join salaries as salaries_manager on salaries_manager.emp_no = dept_manager.emp_no
where 
    salaries.to_date >= now()
    and dept_manager.to_date >= now()
    and salaries_manager.to_date >= now()
    and salaries.salary > salaries_manager.salary
order by  employees.emp_no;    


