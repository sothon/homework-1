

insert into students (name) 
values ('Luke'),('Tony'),('Cinderella'),('Spider'),('Iron'),('Captain'),('Ant'),('Iceman'),('Beast'),('Wolverine');

insert into enrolls (student_id,course_id)
values  (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10),
        (1,11),(2,12),(3,13),(4,14),(5,15),(6,16),(7,17),(8,18),(9,19),(10,20),
        (5,20),(6,19),(7,18),(8,17),(9,16),(10,15);

--มคอรสไหนบางทมคนเรยน (หามแสดงชอคอรสซ)
select distinct courses.id, courses.name
from courses
    inner join enrolls on courses.id = enrolls.course_id
order by courses.id

--มคอรสไหนบางทไมมคนเรยน
select distinct courses.id, courses.name, enrolls.student_id 
from courses
    left join enrolls on courses.id = enrolls.course_id
where 
    enrolls.student_id is null;