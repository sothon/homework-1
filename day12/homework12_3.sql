
--จากการบานท 2 ขอ 1 ใหหาวาใครเปนคนสอน และราคาเทาไร (หามแสดงคอรสซ) โดยทใหใชคสงแค statement เดยวเทานน
select distinct courses.id, courses.name as course_name, instructors.name as instructor_name, courses.price
from courses
    inner join enrolls on courses.id = enrolls.course_id
    inner join instructors on instructors.id = courses.teach_by
order by courses.id    
    
--จากการบานท 2 ขอ 2 ใหหาวาใครเปนคนสอน และราคาเทาไร (หามแสดงคอรสซ) โดยทใหใชคสงแค statement เดยวเทานน
select distinct courses.id, courses.name as course_name, instructors.name as instructor_name, courses.price
from courses
    left join enrolls on courses.id = enrolls.course_id
    left join instructors on instructors.id = courses.teach_by
where 
    enrolls.student_id is null

--จากการบานท 3 ขอ 2 ใหเอามาเฉพาะคอรสทมคนสอน
select distinct courses.id, courses.name as course_name, instructors.name as instructor_name, courses.price
from courses
    left join enrolls on courses.id = enrolls.course_id
    left join instructors on instructors.id = courses.teach_by
where 
    enrolls.student_id is null 
    and courses.teach_by is not null 

