let peopleSalary = [
                    { 
                        "id" : 1001, 
                        "firstname" : "Luke", 
                        "lastname" : "Skywalker", 
                        "company" : "Walt Disney", 
                        "salary" : 40000
                    },
                    { 
                        "id" : 1002, 
                        "firstname" : "Tony", 
                        "lastname" : "Stark", 
                        "company" : "Marvel", 
                        "salary" : 1000000
                    },
                    { 
                        "id" : 1003, 
                        "firstname" : "Somchai", 
                        "lastname" : "Jaidee", 
                        "company" : "Love2work", 
                        "salary" : 20000
                    },
                    { 
                        "id" : 1004, 
                        "firstname" : "Mondey D", 
                        "lastname" : "Luffee", 
                        "company" : "One Piece", 
                        "salary" : 9000000
                    }
                ];


let newpeopleSalary = [];
for (let index in peopleSalary) {

    let newSalary = {};
    for (let key in peopleSalary[index]) {

        if (key !== 'company') {

            newSalary[key] = peopleSalary[index][key];

        }

    }
    newpeopleSalary.push(newSalary);

}
console.log(newpeopleSalary);