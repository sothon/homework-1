
const Router = require('koa-router') 

module.exports = (app) => {
    let todoList = require('../controllers/todoController');
    
    const router = new Router()   
        .get('/todo', todoList.list)
        .post('/todo', todoList.create)
        .get('/todo/:id', todoList.get)
        .patch('/todo/:id', todoList.update)
        .delete('/todo/:id', todoList.remove)
        .put('/todo/:id/complete', todoList.complete)
        .delete('/todo/:id/incomplete', todoList.incomplete)

    return router.routes()    
}
