const Koa = require('koa');
const app = new Koa();

//const render = require('koa-ejs');
//const path = require('path'); 

app.use(requestLogger);
app.use(handler);


app.listen(3000);

async function handler (ctx) {
    console.log('bbb');
    ctx.body = 'hello, koa'
}

async function requestLogger (ctx, next) {
    const start = process.hrtime()
    await next()
    const diff = process.hrtime(start)
    console.log(`${ctx.method} ${ctx.path} ${diff[0] * 1e9 + diff[1]}ns`)
    // console.log('aaa');
    // await next()
    // console.log('ccc');
}