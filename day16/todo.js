const Koa = require('koa') 
const cors = require('koa2-cors');
const render = require('koa-ejs');
const bodyParser = require('koa-bodyparser');

const app = new Koa();
app.use(bodyParser());
app.use(cors());

const router = require('./routes/todoRoute')(app);    
app.use(router)

app.listen(3000)