const todoList = require('../repository/todoList')
const pool = require('../lib/pool');

exports.list = async function (ctx, next)  {
    const db = await pool.getConnection()
    try {
        let todos = await todoList.findAll(db)
        ctx.body = todos
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
};

exports.create = async function (ctx, next)  {
    const db = await pool.getConnection()
    // try {
        todoList.name = ctx.request.body.name
        todoList.status = '0'
        if (todoList.name) {
            const todo = await todoList.store(db, todoList)
            // console.log(insertId)
            ctx.body = todo
        } else {
            ctx.body = { 
                success:false,
                data:{}
            }
        }
        
        
        await next();
    // } catch (err) {
    //     ctx.status = 400
    //     ctx.body = err.message
    // } finally {
        db.release()
    // }
};

exports.get = async function (ctx, next)  {
    const db = await pool.getConnection()
    try {
        const todos = await todoList.find(db, ctx.params.id)
        ctx.body = todos
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
};

exports.update = async function (ctx, next)  {
    const db = await pool.getConnection()
    try {
        const todo = await todoList.find(db, ctx.params.id)
        todo.name = ctx.request.body.name
        await todoList.store(db, todo)
        ctx.body = { 
            success:true,
            data:{            
                name: ctx.request.body.name
            }
        }
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
};

exports.remove = async function (ctx, next)  {
    const db = await pool.getConnection()
    try {
        await todoList.remove(db, ctx.params.id)
        ctx.body = { 
            success:true
        }
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
};

exports.complete = async function (ctx, next)  {
    const db = await pool.getConnection()
    try {
        const todo = await todoList.find(db, ctx.params.id)
        todo.status = '1'
        await todoList.store(db, todo)
        ctx.body = { 
            success:true,
            data:{            
                status: '1'
            }
        }
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
};

exports.incomplete = async function (ctx, next)  {
    const db = await pool.getConnection()
    try {
        const todo = await todoList.find(db, ctx.params.id)
        todo.status = '0'
        await todoList.store(db, todo)
        ctx.body = { 
            success:true,
            data:{            
                status: '0'
            }
        }
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }
};
