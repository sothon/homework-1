class TodoList {
    constructor () {
        this.id
        this.name
        this.status
        this.create_at
    }

    createEntity (row) {
        return {
            id: row.id,
            name: row.name,
            status: row.status,
            created_at: row.created_at
        }
    }
}

module.exports.find = 
        async function (db, id) {
            const [rows] = await db.execute(`select id, name, status, created_at from todolist where id = ?`, [id])
            return new TodoList().createEntity(rows[0])
        }
module.exports.findAll =         
        async function (db) {
            const [rows] = await db.execute(`select id, name, status, created_at from todolist`)
            return rows.map((row) => new TodoList().createEntity(row))
        }        
module.exports.store = 
        async function (db, todoList) {
            if (!todoList.id) {
                // await db.beginTransaction()
                const { insertId } = await db.execute(`insert into todolist (name, status) values (?, ?)`, [todoList.name, todoList.status])
                // await db.commit()                
                const [rows] = await db.execute(`select id, name, status, created_at from todolist order by id desc limit 1`)
                return new TodoList().createEntity(rows[0])
                //return insertId
            }
            return db.execute(`update todolist set name = ?, status = ? where id = ?`, [todoList.name, todoList.status, todoList.id])
        }        
module.exports.remove =   
        async function (db, id) {
            return db.execute(`delete from todolist where id = ?`, [id])
        } 