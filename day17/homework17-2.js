const Koa = require('koa')
const session = require('koa-session')
const mysql = require('mysql2/promise')
let _ = require('lodash');

const pool = mysql.createPool({
    host: '127.0.01',
    user: 'root',
    password: 'root',
    port: '3306',
    database: 'codecamp'
});

const sessionStore = {}

const sessionConfig = {
    key: 'sess',
    maxAge: 3600 * 1000,
    httpOnly: true,
    store: {
        async get (key, maxAge, { rolling }) {
            const db = await pool.getConnection()   
            const [rows] = await db.execute(`select session_key, session_value, created_at from session where session_key = ?`, [key])
            db.release()
            if (!_.isEmpty(rows)) {
                return JSON.parse(rows[0].session_value)
            }    
            // const [rows] = await db.execute(`select session_key, session_value, created_at from session`)
            // db.release()
            // if (!_.isEmpty(rows)) {
            //     sessionStore[rows[0].session_key] = rows[0].session_value
            //     return sessionStore[key]
            // }          
        },
        async set (key, sess, maxAge, { rolling }) {     
            const db = await pool.getConnection()   
            let sess_data = JSON.stringify(sess)
            await db.execute(`insert into session (session_key, session_value) values (?, ?) on duplicate key update session_value = ?`, [key, sess_data, sess_data])
            db.release()
            sessionStore[key] = sess
        },
        destroy (key) {
            delete sessionStore[key]
        }
    }
}

const app = new Koa()

app.keys = ['supersecret']
app
    .use(session(sessionConfig, app))
    .use(handler)
    .listen(3000)

async function handler (ctx) {
    let n = ctx.session.views || 0
    
    ctx.session.views = ++n
    // ctx.body = `${n} views`
    // console.log(sessionStore.sid)
    try {        
        //const session = require('./repository/session')
        // if (!_.isEmpty(sessionStore)) {
        //     const exist_session = await session.find(db, Object.keys(sessionStore)[0])
        //     exist_session.session_key = Object.keys(sessionStore)[0]
        //     exist_session.session_value = JSON.stringify(sessionStore[exist_session.session_key])
        //     await session.store(db, exist_session)
        // }
        ctx.body = sessionStore
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        // db.release()
    }

}    

