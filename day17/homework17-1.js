const Koa = require('koa')
const session = require('koa-session')
const mysql = require('mysql2/promise')
let _ = require('lodash');

const sessionStore = {}

const sessionConfig = {
    key: 'sess',
    maxAge: 3600 * 1000,
    httpOnly: true,
    store: {
        get (key, maxAge, { rolling }) {
            return sessionStore[key]
        },
        set (key, sess, maxAge, { rolling }) {
            sessionStore[key] = sess
            
        },
        destroy (key) {
            delete sessionStore[key]
        }
    }
}

const app = new Koa()

app.keys = ['supersecret']
app
    .use(session(sessionConfig, app))
    .use(handler)
    .listen(3000)

async function handler (ctx) {
    let n = ctx.session.views || 0
    
    ctx.session.views = ++n
    // ctx.body = `${n} views`
    // console.log(sessionStore.sid)
    const pool = mysql.createPool({
        host: 'localhost',
        user: 'root',
        password: 'root',
        port: '3306',
        database: 'codecamp'
    });

    const db = await pool.getConnection()
    try {        
        const session = require('./repository/session')
        if (!_.isEmpty(sessionStore)) {
            const exist_session = await session.find(db, Object.keys(sessionStore)[0])
            exist_session.session_key = Object.keys(sessionStore)[0]
            exist_session.session_value = JSON.stringify(sessionStore[exist_session.session_key])
            await session.store(db, exist_session)
        }
        ctx.body = sessionStore
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {
        db.release()
    }

}    

