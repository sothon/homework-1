let _ = require('lodash');

class Session {
    constructor () {
        this.session_key
        this.session_value
        this.created_at
    }

    createEntity (row) {
        return {
            session_key: row.session_key,
            session_value: row.session_value,
            created_at: row.created_at
        }
    }
}

module.exports.find = 
        async function (db, key) {
            if (key) {
                const [rows] = await db.execute(`select session_key, session_value, created_at from session where session_key = ?`, [key])
                if (!_.isEmpty(rows)) {
                    return new Session().createEntity(rows[0])
                } else {
                    return new Session()
                }
            }
            return
        }
module.exports.findAll =         
        async function (db) {
            const [rows] = await db.execute(`select session_key, session_value, created_at from session`)
            return rows.map((row) => new Session().createEntity(row))
        }        
module.exports.store = 
        async function (db, session) {
            const [rows] = await db.execute(`select session_key, session_value, created_at from session where session_key = ?`, [session.session_key])
            if (_.isEmpty(rows)) {
                const result = await db.execute(`insert into session (session_key, session_value) values (?, ?)`, [session.session_key, session.session_value])
                session.session_key = result.insertId
                return
            } else {
                return db.execute(`update session set session_value = ? where session_key = ?`, [session.session_value, session.session_key])
            }
        }        
module.exports.remove =   
        async function (db, key) {
            return db.execute(`delete from session where session_key = ?`, [key])
        } 