
import {domutil as dom} from "./libs" 

// let hello = () => console.log('Hello World')
// hello()

let addbtn = document.getElementById("btnAdd")
addbtn.addEventListener("click", addItem);
let removebtn = document.getElementsByClassName("btnRemove")
let editbtn = document.getElementsByClassName("btnEdit")
let i = 0;
for (i = 0; i < removebtn.length; i++) {
    removebtn[i].addEventListener("click", removeItem);
    editbtn[i].addEventListener("click", editItem);
}
// console.log(addbtn)
// console.log(removebtn)

function addItem() {
    
    let dataId = document.getElementById("dataId").value
    let title = document.getElementById("dataTitle").value
    let salePrice = document.getElementById("dataSalePrice").value
    let promotionDate = document.getElementById("dataPromotionDate").value
    let image = document.getElementById("dataImage").value
    let itemslist = document.getElementById("itemslist")    

    if (document.getElementById("myModalLabel").innerHTML === 'Add Data') {
        
        let newitem = document.createElement('div')   
        newitem.className = "col-sm-6 col-md-3"
        newitem.setAttribute("id", "box_"+dataId);
        newitem.setAttribute("data-id", dataId);
        newitem.innerHTML = `   
            <div class="card items">
                <div class="card-body">
                    <h5 id="title_${dataId}">${title}</h5>
                    <img class="img-responsive card-image" id="image_${dataId}" src="./images/${image}">
                    <div class="card-text" id="price_${dataId}">Price:${salePrice}$</div>
                    <a href="#" class="btn btn-primary btnEdit" data-id="${dataId}" data-toggle="modal" data-target="#myModalNorm">Edit</a>
                    <a href="#" class="btn btn-danger btnRemove" data-id="${dataId}" data-toggle="confirmation" data-singleton="true">Remove</a>
                </div>
            </div>    
        `
    
        itemslist.appendChild(newitem) 
        
        removebtn = document.getElementsByClassName("btnRemove")
        editbtn = document.getElementsByClassName("btnEdit")
        for (i = 0; i < removebtn.length; i++) {
            if (removebtn[i].getAttribute("data-id") === dataId) {
                removebtn[i].addEventListener("click", function() {            
                    let r = confirm("Confirm to Remove data!!");
                    if (r === true) {
                        let removeItem = document.getElementById("box_"+dataId)
                        itemslist.removeChild(removeItem)     
                    }    
                });
            }
            if (editbtn[i].getAttribute("data-id") === dataId) {
                editbtn[i].addEventListener("click", function() {                    
                    document.getElementById('myModalLabel').innerHTML = 'Edit Data'  
                    document.getElementById("dataId").value = dataId
                    document.getElementById("dataTitle").value = document.getElementById("title_"+dataId).innerHTML
                    document.getElementById("dataSalePrice").value = document.getElementById("price_"+dataId).innerHTML
                    document.getElementById("dataPromotionDate").value = ''
                    document.getElementById("dataImage").value = document.getElementById("image_"+dataId).src                    
                });
            }        
        }
    } else if (document.getElementById("myModalLabel").innerHTML === 'Edit Data') {
        if (title !== '') document.getElementById("title_"+dataId).innerHTML = title
        if (image !== '') document.getElementById("image_"+dataId).src = image
        if (salePrice !== '') document.getElementById("price_"+dataId).innerHTML = salePrice
        document.getElementById('myModalLabel').innerHTML = 'Add Data'    
    }

    document.getElementById("dataId").value = ''
    document.getElementById("dataTitle").value = ''
    document.getElementById("dataSalePrice").value = ''
    document.getElementById("dataPromotionDate").value = ''
    document.getElementById("dataImage").value = ''  
}

function editItem() {
    let dataId = this.getAttribute("data-id")
    document.getElementById('myModalLabel').innerHTML = 'Edit Data'    
    document.getElementById("dataId").value = dataId
    document.getElementById("dataTitle").value = document.getElementById("title_"+dataId).innerHTML
    document.getElementById("dataSalePrice").value = document.getElementById("price_"+dataId).innerHTML
    document.getElementById("dataPromotionDate").value = ''
    document.getElementById("dataImage").value = document.getElementById("image_"+dataId).src

}

function removeItem() {
    let r = confirm("Confirm to Remove data!!");
    if (r === true) {
        let dataId = this.getAttribute("data-id")
        // alert(dataId)
        let removeItem = document.getElementById("box_"+dataId)
        itemslist.removeChild(removeItem)     
    }         
}

