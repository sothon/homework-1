/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__libs__ = __webpack_require__(1);

 

// let hello = () => console.log('Hello World')
// hello()

let addbtn = document.getElementById("btnAdd")
addbtn.addEventListener("click", addItem);
let removebtn = document.getElementsByClassName("btnRemove")
let editbtn = document.getElementsByClassName("btnEdit")
let i = 0;
for (i = 0; i < removebtn.length; i++) {
    removebtn[i].addEventListener("click", removeItem);
    editbtn[i].addEventListener("click", editItem);
}
// console.log(addbtn)
// console.log(removebtn)

function addItem() {
    
    let dataId = document.getElementById("dataId").value
    let title = document.getElementById("dataTitle").value
    let salePrice = document.getElementById("dataSalePrice").value
    let promotionDate = document.getElementById("dataPromotionDate").value
    let image = document.getElementById("dataImage").value
    let itemslist = document.getElementById("itemslist")    

    if (document.getElementById("myModalLabel").innerHTML === 'Add Data') {
        
        let newitem = document.createElement('div')   
        newitem.className = "col-sm-6 col-md-3"
        newitem.setAttribute("id", "box_"+dataId);
        newitem.setAttribute("data-id", dataId);
        newitem.innerHTML = `   
            <div class="card items">
                <div class="card-body">
                    <h5 id="title_${dataId}">${title}</h5>
                    <img class="img-responsive card-image" id="image_${dataId}" src="./images/${image}">
                    <div class="card-text" id="price_${dataId}">Price:${salePrice}$</div>
                    <a href="#" class="btn btn-primary btnEdit" data-id="${dataId}" data-toggle="modal" data-target="#myModalNorm">Edit</a>
                    <a href="#" class="btn btn-danger btnRemove" data-id="${dataId}" data-toggle="confirmation" data-singleton="true">Remove</a>
                </div>
            </div>    
        `
    
        itemslist.appendChild(newitem) 
        
        removebtn = document.getElementsByClassName("btnRemove")
        editbtn = document.getElementsByClassName("btnEdit")
        for (i = 0; i < removebtn.length; i++) {
            if (removebtn[i].getAttribute("data-id") === dataId) {
                removebtn[i].addEventListener("click", function() {            
                    let r = confirm("Confirm to Remove data!!");
                    if (r === true) {
                        let removeItem = document.getElementById("box_"+dataId)
                        itemslist.removeChild(removeItem)     
                    }    
                });
            }
            if (editbtn[i].getAttribute("data-id") === dataId) {
                editbtn[i].addEventListener("click", function() {                    
                    document.getElementById('myModalLabel').innerHTML = 'Edit Data'  
                    document.getElementById("dataId").value = dataId
                    document.getElementById("dataTitle").value = document.getElementById("title_"+dataId).innerHTML
                    document.getElementById("dataSalePrice").value = document.getElementById("price_"+dataId).innerHTML
                    document.getElementById("dataPromotionDate").value = ''
                    document.getElementById("dataImage").value = document.getElementById("image_"+dataId).src                    
                });
            }        
        }
    } else if (document.getElementById("myModalLabel").innerHTML === 'Edit Data') {
        if (title !== '') document.getElementById("title_"+dataId).innerHTML = title
        if (image !== '') document.getElementById("image_"+dataId).src = image
        if (salePrice !== '') document.getElementById("price_"+dataId).innerHTML = salePrice
        document.getElementById('myModalLabel').innerHTML = 'Add Data'    
    }

    document.getElementById("dataId").value = ''
    document.getElementById("dataTitle").value = ''
    document.getElementById("dataSalePrice").value = ''
    document.getElementById("dataPromotionDate").value = ''
    document.getElementById("dataImage").value = ''  
}

function editItem() {
    let dataId = this.getAttribute("data-id")
    document.getElementById('myModalLabel').innerHTML = 'Edit Data'    
    document.getElementById("dataId").value = dataId
    document.getElementById("dataTitle").value = document.getElementById("title_"+dataId).innerHTML
    document.getElementById("dataSalePrice").value = document.getElementById("price_"+dataId).innerHTML
    document.getElementById("dataPromotionDate").value = ''
    document.getElementById("dataImage").value = document.getElementById("image_"+dataId).src

}

function removeItem() {
    let r = confirm("Confirm to Remove data!!");
    if (r === true) {
        let dataId = this.getAttribute("data-id")
        // alert(dataId)
        let removeItem = document.getElementById("box_"+dataId)
        itemslist.removeChild(removeItem)     
    }         
}



/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__dom__ = __webpack_require__(2);
/* unused harmony reexport domUtil */

//export { bomuti } from "./bom";

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class domUtil{
    static setText(id,text){
        let elem=document.getElementById(id)
        elem.innerText=text
    }
     getText(id){
         let elem = document.getElementById(id)
         return elem.innerText
     }
     static openWindow () {
        this.win = window.open("list.html", "",
        "resizable=yes,top=100,left=500,width=500,height=600")
        }
        static closeWindow () {
        if (this.win) {
        this.win.close()
        }
    }

}
/* unused harmony export domUtil */


/***/ })
/******/ ]);