// const mysql = require('mysql2/promise')

// const pool = mysql.createPool({
//   host: 'localhost',
//   user: 'root',
//   password: 'root',
//   port: '3306',
//   database: 'db1'
// });

class Database {

    constructor() {
       
        //this.connection = this.myConnection();
        const mysql = require('mysql2/promise')

        const pool = mysql.createPool({
          host: 'localhost',
          user: 'root',
          password: 'root',
          port: '3306',
          database: 'db1'
        });

        const db = await pool.getConnection()
    }
    async myConnection() {
        const mysql = require('mysql2/promise');
        const myConfig = require('../config/database.js');
        const myconnection  = await mysql.createPool({
            connectionLimit : 10,
            host: 'localhost',
            user: 'root',
            password: 'root',
            database: 'db1'
          });
          
        return myconnection.pool;
    }
    // async exec(conn,sql) {
    //     let rows = await conn.execute(sql);
    //     return rows;
    // }
}

module.exports.db = new Database().connection;