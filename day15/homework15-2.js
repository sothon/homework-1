//insert into users (username, first_name, last_name) values ('12334','abccc','tyuiop')

const mysql = require('mysql2/promise')

const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: '3306',
  database: 'db1'
});

(async function query () {

  const User = require('./repository/user')

  const db = await pool.getConnection()
  await db.beginTransaction()
  try {
    //const userActive = await User.find(db, 4)
    const userActive = await User.findAll(db)
    console.log(userActive[0])
    console.log(userActive[1])
    console.log(userActive[2])
    //console.log(userActive[1])  
    //userActive.firstName = 'hacker'
    //console.log(userActive)
    //await User.store(db, userActive)
    //await db.commit()

    const [rows] = await db.execute(`select id, first_name, last_name from users where id = ?`, [4])
    console.log(rows)

  } catch (err) {
    console.log(err)
    await db.rollback()
  }
  await db.release()
})().then(() => {}, (err) => { 
    console.log(err) 
})
