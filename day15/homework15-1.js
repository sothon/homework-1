//const Koa = require('koa');
//const app = new Koa();
//insert into users (username, first_name, last_name) values ('12334','abccc','tyuiop')

const mysql = require('mysql2/promise')

const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: '3306',
  database: 'db1'
});

(async function () {
  const db = await pool.getConnection()
  //const db = require('./lib/db.js')
//   const db = await pool.getConnection()
    //console.log(db)
  await db.beginTransaction()
  try {

    // const User = require('./model/user')(db)

    // //  const user1 = await User.find(1)
    // const user1 = await User.findByUsername('makro')
    // user1.firstName = 'aaaaaaa'
    // await user1.save()

    // const user2 = await User.find(1)
    // await user2.remove()

    // await db.commit()

    const [rows] = await db.execute(`select id, first_name, last_name from users where id = ?`, [4])
    console.log(rows)

  } catch (err) {
    console.log(err)
    await db.rollback()
  }
  await db.release()
})().then(() => {}, (err) => { 
    console.log(err) 
})
