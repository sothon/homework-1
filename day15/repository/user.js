class User {
    constructor () {
        this.id
        this.firstName
        this.lastName
    }

    async createEntity (row) {
        return {
            id: row.id,
            firstName: row.first_name,
            lastName: row.last_name
        }
    }
}

module.exports.find = 
        async function (db, id) {
            const [rows] = await db.execute(`select id, first_name, last_name from users where id = ?`, [id])
            return new User().createEntity(rows[0])
        }
module.exports.findAll =         
        async function (db) {
            const [rows] = await db.execute(`select id, first_name, last_name from users`)
            return rows.map((row) => new User().createEntity(row))
        }        
module.exports.store = 
        async function (db, user) {
            if (!user.id) {
                const result = await db.execute(`insert into users (first_name, last_name) values (?, ?)`, [user.firstName, user.lastName])
                user.id = result.insertId
                return
            }
            return db.execute(`update users set first_name = ?, last_name = ? where id = ?`, [user.firstName, user.lastName, user.id])
        }        
module.exports.remove =   
        async function (db, id) {
            return db.execute(`delete from users where id = ?`, [id])
        }      


// module.exports.User = function () {
//     return {
//         async find (db, id) {
//             const [rows] = await db.execute(`select id, first_name, last_name from users where id = ?`, [id])
//             // return new User(db, rows[0])
//             return new User().createEntity(rows[0])
//         },
//         async findByUsername (username) {
//             const [rows] = await db.execute(`select id, first_name, last_name from users where username = ?`, [username])
//             return new User(db, rows[0])
//         },
//         async findAll () {
//             const [rows] = await this.db.execute(`select id, first_name, last_name from users`)
//             return rows.map((row) => new User(db, row))
//         }
//     }
// }