const {Employee}  = require('./employee');
const {CEO}  = require('./ceo');
    
let dang = new Employee('Dang','Red', 10000);
console.log(dang);
dang.hello();
console.log(dang.getSalary());

let ceo = new CEO('Somchai', 'Sudlor', 30000);
console.log(ceo);
ceo.hello();
console.log(ceo.getSalary());