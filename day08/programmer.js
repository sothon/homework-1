const {Employee}  = require('./employee');

class Programmer extends Employee {
    constructor(firstname, lastname, salary, id, type) {
        super(firstname, lastname, salary);
        this.id = id;
        this.type = type;
    }
    work() {  // simulate public method
        this._CreateWebsite();
        this._FixPC();
        this._InstallWindows();
    };
    _CreateWebsite() { // simulate public method
        console.log("CreateWebsite!");
    };
    _FixPC() { // simulate public method
        console.log("FixPC!");
    };
    _InstallWindows() { // simulate public method
        console.log("InstallWindows!");
    };

}

exports.Programmer = Programmer;
