const {CEO}  = require('./ceo');
const {Programmer}  = require('./programmer');

async function start() {

    let somchai = new CEO("Somchai", "Sudlor", 30000);

    // let employees  = await somchai.getEmployee();
    let employees  = await somchai.employeesRaw;

    let programmers = employees.map((employee) => {
        return new Programmer(
            employee['firstname'], 
            employee['lastname'], 
            employee['salary'], 
            employee['id'],
            'frontend',
        );    
    });
    
    somchai.employees = programmers;
    programmers[0].work();

    console.log('CEO => ', somchai);
    console.log('PROGRAMMERS => ', programmers);
}

start();