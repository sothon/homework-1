const {Employee}  = require('./employee.js');
let readData  = require('./readData');

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
        this.employeesRaw = this.getEmployeesRaw();
        // this.employeesRaw = JSON.parse(this.getEmployeesRaw());
        this.employees;
        /*(async () => {
            super(firstname, lastname, salary);
            this.dressCode = 'suit';
            // let aa = JSON.parse(this.getEmployeesRaw());
            // console.log(aa);
            this.employeesRaw = this.getEmployeesRaw();
            this.employees;
        })();
        */
    };
    getSalary(){  // simulate public method
        return super.getSalary()*2;
    };
    work (employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    };
    increaseSalary(employee, newSalary) {

        if (employee.setSalary(newSalary)){
            console.log(''+employee.firstname+'\'s salary has been set to '+newSalary);
        } else {
            console.log(''+employee.firstname+'\'s salary is less than before!!');
        }
        
    };
    _golf () { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
    };
    _seminar () { 
        console.log('He is going to seminar Dress with :' + this.dressCode);
    };
    _fire (employee) { 
        console.log(employee.firstname + 'has been fired! Dress with :' + employee.dressCode);
    };
    _hire (employee) { 
        console.log(employee.firstname + 'has been hired back! Dress with :' + employee.dressCode);
    };
    gossip(employee, message) {
        console.log('Hey ' + employee.firstname + ',' + message);
    };
    async getEmployeesRaw() {
        const data = await readData.readData('homework1.json');
        let employees  = JSON.parse(data);
        return employees;
    }
}

exports.CEO = CEO;
