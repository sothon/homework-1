function hello(parameter1, parameter2, parameter3) {
    console.log('hello' + parameter1 + parameter2 + parameter3);
}; // Declaration
    
let hello2 = function(parameter1, parameter2, parameter3) {
    console.log('hello' + parameter1 + parameter2 + parameter3);
}; // Declaration
    
hello ('1','2','3'); // Execution
hello2 ('a','b','c'); // Execution

let hello3 = function() {
    parameter1 = '1';
    parameter2 = '2';
    parameter3 = 'c';
    
    console.log('hello' + parameter1 + parameter2 + parameter3);
}; // Declaration
    
hello3 ('1','2','3'); // Execution

function func(callback) {
    callback('x','y','z');
} // Declaration
    
func(function(parameter1, parameter2, parameter3) { // Declaration
    console.log('hello' + parameter1 + parameter2 + parameter3);
}); // Execution

function func2() {
    callback = function() { // Declaration
        parameter1 = 'x1x';
        parameter2 = '2';
        parameter3 = '3';
    
        console.log('hello' + parameter1 + parameter2 + parameter3);
    };
    callback('1','2','3'); // Execution
    
} // Declaration
    
func2(); // Execution
    
let hello4 = function(parameter1, parameter2, callbackFunction) {
    console.log('hello' + parameter1 + parameter2);
    callbackFunction(); // Execution
}; // Declaration
    
hello4('1','2',function(){ // Declaration
    console.log('Inside Callback function');
}); // Execution


function helloX() {
    return 1;
    }
    
//console.log(helloX); // Function
//console.log(helloX()); //1

function tryHelloX(callbackFunction) {
    return callbackFunction();
}
console.log(helloX()); // 1
console.log(helloX); // Function
console.log( tryHelloX(helloX) ); // 1
console.log( tryHelloX(helloX()) ); // ?