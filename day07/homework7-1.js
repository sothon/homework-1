let readData  = require('./readData');
let addYearSalary  = require('./addYearSalary');
let addNextSalary  = require('./addNextSalary');


async function addAdditionalFields(employees) {
    // STEP 3 //
    let newEmployees = employees.map((row) => {
        return addYearSalary.addYearSalary(row);
    })
    .map((row) => {
        return addNextSalary.addNextSalary(row);
    })  
    newEmployees[0]['salary'] = 0;
    console.log(employees);
    console.log('--------');
    console.log(newEmployees);
}


async function start() {
    // STEP 2 //
    const data = await readData.readData('homework1.json');
    employees  = JSON.parse(data);
    addAdditionalFields(employees);

}

// STEP 1 //
start();