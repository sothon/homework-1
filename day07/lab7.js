const Koa = require('koa');
const logger = require('koa-logger');
let winston = require('winston');
const MESSAGE = Symbol.for('message');
const app = new Koa();

app.use(async (ctx, next) => {
    try {
        await next();
        ctx.body = 'Hello World';
        winLog.info(ctx.body);
        someUnknownFunction();
    } catch (err) {
        ctx.status = 400
        ctx.body = `Uh-oh: ${err.message}`
        //console.log('Error handler:', err.message)
        winLog.error('Error handler:' + err.message);
    }
})

const jsonFormatter = (logEntry) => {
    const base = { timestamp: new Date() };
    const json = Object.assign(base, logEntry)
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
}

const winLog = winston.createLogger({
    level: 'info',
    // format: winston.format.json(),
    format: winston.format(jsonFormatter)(),
    transports: [
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' })
        ]
    });
    winLog.add(new winston.transports.Console({
        format: winston.format.simple(),
    }));

//winLog.info('This is info log');
//winLog.error('This is error log');

app.listen(3000);
app.use(logger());