
function cloneObject(object) {

    if (Array.isArray(object)) {
        let newArray = [];
        let keys = object.map((value) => {
            if (typeof(value) === 'object') {
                newArray.push(cloneObject(value));
            } else {
                newArray.push(value);
            }  
        });
        return newArray;

    } else {
        let newObject = {};
        let keys = Object.keys(object).map((key) => {
            if (typeof(object[key]) === 'object') {    
                newObject[key] = cloneObject(object[key]);
            } else {
                newObject[key] = object[key]; 
            }
        });
        return newObject;

    }

}

exports.cloneObject = cloneObject;