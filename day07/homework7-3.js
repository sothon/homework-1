let input1 = [1,2,3];
let input2 = {a:1,b:2};
let input3 = [1,2,{a:1,b:2}];
let input4 = [1,2,{a:1,b:{c:3,d:4}}];
let input5 = [1,2,{a:1,b:[3,4]}];
// let input6 = [1,2,{a:1,b:[3,4,{c:5,d:6}]}];

let cloneObject  = require('./cloneObject');

let newInput1 = cloneObject.cloneObject(input1);
newInput1[0] = 100; 
console.log('input1    => ', input1);
console.log('newInput1 => ', newInput1);

let newInput2 = cloneObject.cloneObject(input2);
newInput2.a = 200; 
console.log('input2    => ', input2);
console.log('newInput2 => ', newInput2);

let newInput3 = cloneObject.cloneObject(input3);
newInput3[2].a = 300; 
console.log('input3    => ', input3);
console.log('newInput3 => ', newInput3);

let newInput4 = cloneObject.cloneObject(input4);
newInput4[1] = 200; 
newInput4[2].b.c = 400; 
console.log('input4    => ', input4);
console.log('newInput4 => ', newInput4);

let newInput5 = cloneObject.cloneObject(input5);
newInput5[1] = 200; 
newInput5[2].b[0] = 500; 
console.log('input5    => ', input5);
console.log('newInput5 => ', newInput5);

// let newInput6 = cloneObject.cloneObject(input6);
// newInput6[1] = 200; 
// newInput6[2].b[1] = 600; 
// newInput6[2].b[2].c = 1600; 
// console.log('input6    => ', input6);
// console.log('newInput6 => ', newInput6);
// console.log(newInput6);




