function addYearSalary(employee) {
    
    employee['yearSalary'] = employee['salary'] * 12;

    return employee;

}

exports.addYearSalary = addYearSalary;