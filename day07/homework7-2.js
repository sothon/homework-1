let readData  = require('./readData');
let cloneObject  = require('./cloneObject');
let addYearSalary  = require('./addYearSalary');
let addNextSalary  = require('./addNextSalary');


async function addAdditionalFields(employees) {

    let newEmployees = employees
    .map((employee) => {
        return cloneObject.cloneObject(employee);
    })
    .map((employee) => {
        return addYearSalary.addYearSalary(employee);
    })
    .map((employee) => {
        return addNextSalary.addNextSalary(employee);
    })  
    newEmployees[0]['salary'] = 0;
    console.log(employees);
    console.log('--------');
    console.log(newEmployees);
}


async function start() {

    const data = await readData.readData('homework1.json');
    employees  = JSON.parse(data);
    addAdditionalFields(employees);

}

start();