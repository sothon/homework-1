/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "dist";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _libs = __webpack_require__(1);

// let hello = () => console.log('Hello World')
// hello()

var addbtn = document.getElementById("btnAdd");
addbtn.addEventListener("click", addItem);

$('#myModalNorm').on('hidden.bs.modal', function () {
    document.getElementById('myModalLabel').innerHTML = 'Add Data';
    document.getElementById("dataId").value = '';
    document.getElementById("dataTitle").value = '';
    document.getElementById("dataSalePrice").value = '';
    document.getElementById("dataPromotionDate").value = '';
    document.getElementById("dataImage").value = '';
});

getDataList();

function getDataList() {
    var url = 'http://localhost:3000/book';
    fetch(url, {
        method: 'GET', // or 'PUT'
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then(function (res) {
        return res.json();
    }).catch(function (error) {
        return console.error('Error:', error);
    }).then(function (response) {
        console.log('Success:', response);
        response.forEach(function (element) {
            // console.log(element.isbn)
            var newitem = document.createElement('div');
            newitem.className = "col-sm-6 col-md-3";
            newitem.setAttribute("id", "box_" + element.isbn);
            newitem.setAttribute("data-id", element.isbn);
            newitem.innerHTML = "   \n                    <div class=\"card items\">\n                        <div class=\"card-body\">\n                            <h5 id=\"title_" + element.isbn + "\">" + element.book_name + "</h5>\n                            <img class=\"img-responsive card-image\" id=\"image_" + element.isbn + "\" src=\"" + element.pic + "\">\n                            <div class=\"card-text\" id=\"price_" + element.isbn + "\">Price:" + element.price + "</div>\n                            <div class=\"card-text\" id=\"promo_" + element.isbn + "\">" + element.promotion_date + "</div>\n                            <a href=\"#\" class=\"btn btn-primary btnEdit\" data-id=\"" + element.isbn + "\" data-toggle=\"modal\" data-target=\"#myModalNorm\">Edit</a>\n                            <a href=\"#\" class=\"btn btn-danger btnRemove\" data-id=\"" + element.isbn + "\" data-toggle=\"confirmation\" data-title=\"Remove Data?\">Remove</a>\n                        </div>\n                    </div>    \n                ";
            itemslist.appendChild(newitem);
        });

        var editbtn = document.getElementsByClassName("btnEdit");
        var i = 0;
        for (i = 0; i < editbtn.length; i++) {
            editbtn[i].addEventListener("click", editItem);
        }

        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            container: 'body',
            title: 'Remove Content ?',
            onConfirm: function onConfirm() {
                var dataId = $(this).data('id');
                var remove = document.getElementById("box_" + dataId);
                var url = 'http://localhost:3000/book/' + dataId;
                fetch(url, {
                    method: 'DELETE', // or 'PUT'
                    headers: new Headers({
                        'Content-Type': 'application/json'
                    })
                }).then(function (res) {
                    return res.json();
                }).catch(function (error) {
                    return console.error('Error:', error);
                }).then(function (response) {
                    console.log('Success:', response);
                    itemslist.removeChild(remove);
                });
            }
        });
    });
}

function addItem() {

    var dataId = document.getElementById("dataId").value;

    var title = document.getElementById("dataTitle").value;
    var salePrice = document.getElementById("dataSalePrice").value;
    var promotionDate = document.getElementById("dataPromotionDate").value;
    var image = document.getElementById("dataImage").value;
    var itemslist = document.getElementById("itemslist");

    if (document.getElementById("myModalLabel").innerHTML === 'Add Data') {

        var url = 'http://localhost:3000/book';
        var data = {
            isbn: dataId,
            book_name: title,
            price: salePrice,
            promotion_date: promotionDate,
            pic: image
        };
        fetch(url, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }).then(function (res) {
            return res.json();
        }).catch(function (error) {
            return console.error('Error:', error);
        }).then(function (response) {
            console.log('Success:', response);

            var newitem = document.createElement('div');
            newitem.className = "col-sm-6 col-md-3";
            newitem.setAttribute("id", "box_" + response.data.isbn);
            newitem.setAttribute("data-id", response.data.isbn);
            newitem.innerHTML = "   \n                    <div class=\"card items\">\n                        <div class=\"card-body\">\n                            <h5 id=\"title_" + response.data.isbn + "\">" + response.data.book_name + "</h5>\n                            <img class=\"img-responsive card-image\" id=\"image_" + response.data.isbn + "\" src=\"" + response.data.pic + "\">\n                            <div class=\"card-text\" id=\"price_" + response.data.isbn + "\">Price:" + response.data.price + "</div>\n                            <div class=\"card-text\" id=\"promo_" + response.data.isbn + "\">" + response.data.promotion_date + "</div>\n                            <a href=\"#\" class=\"btn btn-primary btnEdit\" data-id=\"" + response.data.isbn + "\" data-toggle=\"modal\" data-target=\"#myModalNorm\">Edit</a>\n                            <a href=\"#\" class=\"btn btn-danger btnRemove\" data-id=\"" + response.data.isbn + "\" data-toggle=\"confirmation\" data-title=\"Remove Data?\">Remove</a>\n                        </div>\n                    </div>    \n                ";
            itemslist.appendChild(newitem);
            var removebtn = document.getElementsByClassName("btnRemove");
            var editbtn = document.getElementsByClassName("btnEdit");
            var i = 0;
            for (i = 0; i < editbtn.length; i++) {
                if (editbtn[i].getAttribute("data-id") === dataId) {
                    $('[data-toggle=confirmation]').confirmation({
                        rootSelector: '[data-toggle=confirmation]',
                        container: 'body',
                        title: 'Remove Content ?',
                        onConfirm: function onConfirm() {
                            var dataId = $(this).data('id');
                            var remove = document.getElementById("box_" + dataId);
                            var url = 'http://localhost:3000/book/' + dataId;
                            fetch(url, {
                                method: 'DELETE', // or 'PUT'
                                headers: new Headers({
                                    'Content-Type': 'application/json'
                                })
                            }).then(function (res) {
                                return res.json();
                            }).catch(function (error) {
                                return console.error('Error:', error);
                            }).then(function (response) {
                                console.log('Success:', response);
                                itemslist.removeChild(remove);
                            });
                        }
                    });
                }
                if (editbtn[i].getAttribute("data-id") === dataId) {
                    editbtn[i].addEventListener("click", function () {
                        document.getElementById('myModalLabel').innerHTML = 'Edit Data';
                        document.getElementById("dataId").value = dataId;
                        document.getElementById("dataTitle").value = document.getElementById("title_" + dataId).innerHTML;
                        var priceVal = document.getElementById("price_" + dataId).innerHTML.split(":");
                        document.getElementById("dataSalePrice").value = priceVal[1];
                        document.getElementById("dataPromotionDate").value = document.getElementById("promo_" + dataId).innerHTML;
                        document.getElementById("dataImage").value = document.getElementById("image_" + dataId).src;
                    });
                }
            }
        });
    } else if (document.getElementById("myModalLabel").innerHTML === 'Edit Data') {
        var _url = 'http://localhost:3000/book/' + dataId;
        var data = {
            isbn: dataId,
            book_name: title,
            price: salePrice,
            promotion_date: promotionDate,
            //pic: document.getElementById("image_"+dataId).src,
            pic: image
        };
        fetch(_url, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }).then(function (res) {
            return res.json();
        }).catch(function (error) {
            return console.error('Error:', error);
        }).then(function (response) {
            console.log('Success:', response.data);
            document.getElementById("title_" + response.data.isbn).innerHTML = response.data.book_name;
            document.getElementById("price_" + response.data.isbn).innerHTML = 'Price:' + response.data.price;
            document.getElementById("promo_" + response.data.isbn).innerHTML = response.data.promotion_date;
            document.getElementById("image_" + response.data.isbn).src = response.data.pic;
        });
        document.getElementById('myModalLabel').innerHTML = 'Add Data';
    }
}

function editItem() {
    var dataId = this.getAttribute("data-id");

    document.getElementById('myModalLabel').innerHTML = 'Edit Data';
    document.getElementById("dataId").value = dataId;
    document.getElementById("dataTitle").value = document.getElementById("title_" + dataId).innerHTML;
    var priceVal = document.getElementById("price_" + dataId).innerHTML.split(":");
    document.getElementById("dataSalePrice").value = priceVal[1];
    document.getElementById("dataPromotionDate").value = document.getElementById("promo_" + dataId).innerHTML;
    document.getElementById("dataImage").value = document.getElementById("image_" + dataId).src;
}

function fetchData(url, method, data) {
    //let url = 'http://localhost:3000/book';
    // var data = {username: 'example'};
    fetch(url, {
        method: method, // or 'PUT'
        //body: JSON.stringify(data),
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then(function (res) {
        return res.json();
    }).catch(function (error) {
        return console.error('Error:', error);
    }).then(function (response) {
        return console.log('Success:', response);
    });
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _dom = __webpack_require__(2);

Object.defineProperty(exports, "domUtil", {
  enumerable: true,
  get: function get() {
    return _dom.domUtil;
  }
});

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var domUtil = exports.domUtil = function () {
    function domUtil() {
        _classCallCheck(this, domUtil);
    }

    _createClass(domUtil, [{
        key: "getText",
        value: function getText(id) {
            var elem = document.getElementById(id);
            return elem.innerText;
        }
    }], [{
        key: "setText",
        value: function setText(id, text) {
            var elem = document.getElementById(id);
            elem.innerText = text;
        }
    }, {
        key: "openWindow",
        value: function openWindow() {
            this.win = window.open("list.html", "", "resizable=yes,top=100,left=500,width=500,height=600");
        }
    }, {
        key: "closeWindow",
        value: function closeWindow() {
            if (this.win) {
                this.win.close();
            }
        }
    }]);

    return domUtil;
}();

/***/ })
/******/ ]);