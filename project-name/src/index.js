import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
// import Button from 'antd/lib/button';

// const Test = function() {
//     return (
//         <div className="TestCenter">
//             <h1>Good moring Teacher</h1>
//             <h2>I,m fine thankyou</h2>
//             <h3>I,m fine and you</h3>
//             <h4>Sit down</h4>
//             <Button type="primary">Button</Button>
//         </div>
//     )
// }
ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(<Test />, document.getElementById('root'));
registerServiceWorker();
