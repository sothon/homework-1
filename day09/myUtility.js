let _ = require('lodash');

class MyUtility {
    constructor() {
        // this.id = id;
        // super.dressCode = dresscode;
    }
    _assign(object, sources) { 
        return _.assign(object, sources);
    };
    _times(n, iteratee) { 
        return _.times(n, iteratee);
    };
    _keyBy(collection, iteratee) { 
        return _.keyBy(collection, iteratee);
    };
    _cloneDeep(value) { 
        return _.cloneDeep(value);
    };
    _filter(collection, predicate) {
        return _.filter(object, predicate);
    }
    _sortBy(collection, iteratees) {
        return _.sortBy(collection, iteratees);
    }

}

exports.MyUtility = MyUtility;
