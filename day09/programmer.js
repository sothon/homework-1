const {Employee}  = require('./employee');

class Programmer extends Employee {
    constructor(firstname, lastname, salary, id, type) {
        super(firstname, lastname, salary);
        this.id = id;
        this.type = type;
    }
    work() {  // simulate public method
        this._createWebsite();
        this._fixPC();
        this._installWindows();
    };
    _createWebsite() { // simulate public method
        console.log("CreateWebsite!");
    };
    _fixPC() { // simulate public method
        console.log("FixPC!");
    };
    _installWindows() { // simulate public method
        console.log("InstallWindows!");
    };
    talk(message) {
        console.log(message);
    }
    reportRobot(self, robotMessage) {
        self.talk(robotMessage);
    }

}

exports.Programmer = Programmer;
