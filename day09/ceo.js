const {Employee}  = require('./employee');
let readData  = require('./readData');

class CEO extends Employee {
    constructor(firstname, lastname, salary, id, dresscode) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
        this.employeesRaw = this.getEmployeesRaw();
        this.employees;
        this.id = id;
        super.dressCode = dresscode;
    };
    
    work () {  // simulate public method
        this._seminar();
        this._hire();
        this._golf()        
        this._fire();
    };
    _golf () { // simulate private method
        console.log("Golf!");
    };
    _seminar () { 
        console.log("Seminar!");
    };
    _fire (employee) { 
        console.log("Fire!");
    };
    _hire (employee) { 
        console.log("Hire!");
    };
    talk(message) {
        console.log(message);
    }
    reportRobot(self, robotMessage) {
        self.talk(robotMessage);
    }
    async getEmployeesRaw() {
        const data = await readData.readData('homework1.json');
        let employees  = JSON.parse(data);
        return employees;
    }
}

exports.CEO = CEO;
