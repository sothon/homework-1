const {MyUtility}  = require('./myUtility');

let myutil = new MyUtility();

let input1 = { 'a': 0 };
let newInput1 = myutil._assign({}, input1);
console.log('assign    => ', input1);
console.log('newassign => ', newInput1);
console.log('-------------');

let newInput2 = myutil._times(5, String);
console.log('times => ', newInput2);
console.log('-------------');

let input3 = [
    { 'dir': 'left', 'code': 97 },
    { 'dir': 'right', 'code': 100 }
  ];
let newInput3 = myutil._keyBy(input3, 'dir');
console.log('newkeyby => ', newInput3);
console.log('-------------');

let input4 = [1,2,{a:1,b:2}];
let newInput4 = myutil._cloneDeep(input4);
newInput4[2].a = 300; 
console.log('cloneDeep    => ', input4);
console.log('newcloneDeep => ', newInput4);
console.log('-------------');

let input5 = [
    { 'user': 'fred',   'age': 48 },
    { 'user': 'barney', 'age': 36 },
    { 'user': 'fred',   'age': 40 },
    { 'user': 'barney', 'age': 34 }
];
let newInput5 = myutil._sortBy(input5, [function(o) { return o.user; }]);
console.log('sortby => ', input5);
console.log('newsortby => ', newInput5);
console.log('-------------');

