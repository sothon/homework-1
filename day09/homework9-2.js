const {CEO}  = require('./ceo');
const {Programmer}  = require('./programmer');
const {OfficeCleaner}  = require('./officeCleaner');
let readData  = require('./readData');

async function start() {
    
    let somchai = new CEO("Somchai", "Sudlor", 30000, '1001', 'tshirt');
    const data = await readData.readData('employee9.json');
    let employees  = JSON.parse(data);
    
    let programmers = employees.map((employee) => {

        if (employee['role'] === 'CEO') {
            return new CEO(employee['firstname'], 
                            employee['lastname'], 
                            employee['salary'], 
                            employee['id'], 
                            employee['dressCode']);    
        } 
        if (employee['role'] === 'Programmer') {
            return new Programmer(employee['firstname'], 
                            employee['lastname'], 
                            employee['salary'], 
                            employee['id'], 
                            employee['type']
                                );    
        } 
        if (employee['role'] === 'OfficeCleaner') {
            return new OfficeCleaner(employee['firstname'], 
                            employee['lastname'], 
                            employee['salary'], 
                            employee['id'], 
                            employee['dressCode']);    
        }
    });
    
    somchai.employees = programmers;

    programmers.map((employee) => {
        employee.work();
        console.log('-----WORK END-----');
    })

    console.log('PROGRAMMERS => ', programmers);
}
start();
