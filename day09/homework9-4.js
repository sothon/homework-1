const {CEO}  = require('./ceo');
const {Programmer}  = require('./programmer');
const {OfficeCleaner}  = require('./officeCleaner');
let readData  = require('./readData');

const EventEmitter = require('events');
const myEmitter = new EventEmitter();

let callback = (officers, robotMessage) => {
    officers.map((officer) => {
        officer.reportRobot(officer,robotMessage);
    })
    
};
    
myEmitter.on('myEvent', callback);

async function start() {
    
    let somchai = new CEO("Somchai", "Sudlor", 30000, '1001', 'tshirt');
    const data = await readData.readData('employee9.json');
    let employees  = JSON.parse(data);
    
    let officers = employees.map((employee) => {

        if (employee['role'] === 'CEO') {
            return new CEO(employee['firstname'], 
                                    employee['lastname'], 
                                    employee['salary'], 
                                    employee['id'], 
                                    employee['dressCode']);    
        } 
        if (employee['role'] === 'Programmer') {
            return new Programmer(employee['firstname'], 
                                    employee['lastname'], 
                                    employee['salary'], 
                                    employee['id'], 
                                    employee['type']
                                );    
        } 
        if (employee['role'] === 'OfficeCleaner') {
            return new OfficeCleaner(employee['firstname'], 
                                    employee['lastname'], 
                                    employee['salary'], 
                                    employee['id'], 
                                    employee['dressCode']);    
        }
    });
    
    somchai.employees = officers;
    let robot = await readData.readData('robot.txt');
    myEmitter.emit('myEvent', officers, robot);   
    
}
start();
