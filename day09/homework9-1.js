const {CEO}  = require('./ceo');
let fs = require('fs');

let somchai = new CEO("Somchai", "Sudlor", 30000, '0001', 'tshirt');

const EventEmitter = require('events');
const myEmitter = new EventEmitter();

let functioncall = (ceo, robotMessage) => {
    somchai.reportRobot(ceo,robotMessage)
};
    
myEmitter.on('myEvent', functioncall);

let robot = '';
fs.readFile('head.txt', 'utf8', function(err, data) {
    
    robot += data + '\n';

    fs.readFile('body.txt', 'utf8', function(err, data) {

        robot += data + '\n';

        fs.readFile('leg.txt', 'utf8', function(err, data) {

            robot += data + '\n';

            fs.readFile('feet.txt', 'utf8', function(err, data) {

                robot += data + '\n';

                fs.writeFile('robot.txt', robot, 'utf8', function(err) {
                    myEmitter.emit('myEvent', somchai, robot);                                
                });            
            });
        });
    });
});
