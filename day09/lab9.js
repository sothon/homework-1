//*****Singleton Example*****//
// class Database {
//     constructor(host, username, password) {
//         this.host = host;
//         this.username = username;
//         this.password = password;
//     }

//     static getInstance() {
//         if (!this._instance)
//         this._instance = new Database('localhost','root','12345');
        
//         return this._instance;
//     }
// }
// let db = Database.getInstance();

//*****Adapter Example*****//
const mysql = require('mysql');
class Database {
    constructor() {
        this.connection = mysql.createConnection({
            host        : 'localhost',
            user        : 'me',
            password    : 'secret',
            database    : 'my_db'
        });
    }
    getUsers(callbackFunction) {
        let sql = "select * from user";
        this.connection.query(sql, function(err ,result) {
            if (err) 
                console.error(err);
            else 
                callbackFunction(result);
        })
    }
}

const db = new Database();
db.getUsers(callbackFunction);
