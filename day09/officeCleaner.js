const {Employee}  = require('./employee');

class OfficeCleaner extends Employee {
    constructor(firstname, lastname, salary, id, dresscode) {
        super(firstname, lastname, salary);
        this.id = id;
        super.dressCode = dresscode;
    }
    work() {  // simulate public method
        this._clean();
        this._killCoachroach();
        this._decorateRoom();
        this._welcomeGuest();
    };
    _clean() { // simulate public method
        console.log("Clean!");
    };
    _killCoachroach() { // simulate public method
        console.log("KillCoachroach!");
    };
    _decorateRoom() { // simulate public method
        console.log("DecorateRoom!");
    };
    _welcomeGuest() { // simulate public method
        console.log("WelcomeGuest!");
    };
    talk(message) {
        console.log(message);
    }
    reportRobot(self, robotMessage) {
        self.talk(robotMessage);
    }

}

exports.OfficeCleaner = OfficeCleaner;
