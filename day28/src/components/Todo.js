import React, { Component } from 'react';
import { Input, Icon, Button, Card, List, Spin, message, Checkbox, Row, Col } from 'antd';
import reqwest from 'reqwest';
import InfiniteScroll from 'react-infinite-scroller';
import './Todo.css';
const fakeDataUrl = 'http://localhost:3000/todo';
const CheckboxGroup = Checkbox.Group;
const plainOptions = ['1', '2'];
const defaultCheckedList = [];


export class Todo extends Component {

  constructor(props) {
    super(props);

    this.state = {
      inputText: '',    
      data: [],
      loading: false,
      hasMore: true,
      checkedList: defaultCheckedList,
      indeterminate: true,
      checkAll: false,
    }

    this.handleChangeText = this.handleChangeText.bind(this);

  }

  getData = (callback) => {
    reqwest({
      url: fakeDataUrl,
      type: 'json',
      method: 'get',
      contentType: 'application/json',
      success: (res) => {
        callback(res);
      },
    });
  }

  componentDidMount() {
    // เราควรจะ fetch เพื่อเอาค่ามาจาก MockAPI 
    //this.fetchGet();
    this.getData((res) => {
      this.setState({
        data: res,
      });
    });
  }

  handleInfiniteOnLoad = () => {
    let data = this.state.data
    this.setState({
      loading: true,
    });
    if (data.length > 6) {
      message.warning('Infinite List loaded all');
      this.setState({
        hasMore: false,
        loading: false,
      });
      return;
    }
    this.getData((res) => {
      data = data.concat(res);
      this.setState({
        data,
        loading: false,
      });
    });
  }

  onChange = (checkedList) => {
    this.setState({
      checkedList,
      indeterminate: !!checkedList.length && (checkedList.length < plainOptions.length),
      checkAll: checkedList.length === plainOptions.length,
    });
  }

  onCheckAllChange = (e) => {
    this.setState({
      checkedList: e.target.checked ? plainOptions : [],
      indeterminate: false,
      checkAll: e.target.checked
    });
  }

  // async fetchGet() {
  //   const result = await fetch('http://localhost:3000/todo')
  //   let listItem = await result.json();
  //   this.setState({ listItem, isLoading: false })
  // }

  async fetchPost(text) {
    this.setState({ isLoading: true });
    const result = await fetch('http://localhost:3000/todo', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: text
      }),
    })

    if (result.ok) {
      // ท่านี้ก็ได้ดูดีกว่า 1
      let data = await result.json()
      // console.log(data.data.name)
      let listItem = this.state.listItem.concat(data);
      this.setState({ listItem, isLoading: false })

      // ท่านี้ก็ได้ดูดีกว่า 2
      //this.fetchGet();

    }

  }

  async fetchDelete(id) {
    const result = await fetch('http://localhost:3000/todo/' + id, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })

    if (result.ok) {
      //let data = await result.json()
      const result = this.state.listItem.filter((item) => {
        if (item.id === id) {
          return false
        } else {
          return true
        }
      });

      console.log(result);
      //result.splice(id, 1);
      this.setState({ listItem: result });
    }
  }

  deleteListAtIndex = (id) => {
    // ไม่ควรทำเพราะเป็นการ Render ใหม่ทั้ง State ถ้ามีเยอะก็ฉิบหายยย สิครับ
    // this.state.listItem.splice(index, 1);
    // this.setState({});
    this.fetchDelete(id);
    // const result = this.state.listItem;
    // result.splice(id, 1);
    // this.setState({listItem: result});
  }

  submitList = () => {
    this.fetchPost(this.state.inputText);
    this.setState({
      //listItem: this.state.listItem.concat([this.state.inputText]),
      inputText: ''
    })
    //console.log(this.state.listItem);
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submitList();
    }
  }

  handleChangeText = (event) => {
    this.setState({ inputText: event.target.value });
  }

  render() {

    // หลัง Return มันต้องมี DIV ครอบก่อน
    // { if 1==1 ? 'TRUE' : 'FALSE'}
    return (
      <div>
        {

          <Card style={{ width: 500, backgroundColor: this.props.myColor }}>
            <h1>To-do-list</h1>

            <div style={{ marginBottom: '10px' }}>
              <Input
                addonAfter={<Button type="primary" onClick={this.submitList}>Add</Button>}
                onChange={this.handleChangeText}
                value={this.state.inputText}
                onKeyPress={this.handleKeyPress} />
            </div>
            <div style={{ borderBottom: '1px solid #E9E9E9' }}>
              <Checkbox
                indeterminate={this.state.indeterminate}
                onChange={this.onCheckAllChange}
                checked={this.state.checkAll}
              >
                Check all
              </Checkbox>
            </div>
            <div className="demo-infinite-container">
              <InfiniteScroll
                initialLoad={false}
                pageStart={0}
                loadMore={this.handleInfiniteOnLoad}
                hasMore={!this.state.loading && this.state.hasMore}
                useWindow={false}
              >
                <List
                  dataSource={this.state.data}
                  renderItem={item => (
                    
                    <List.Item key={item.id} 
                      actions={[<a onClick={() => this.deleteListAtIndex(item.id)}>
                        <Icon type="close-circle" style={{ fontSize: 16, color: 'rgb(255, 145, 0)' }} /></a>]}>
                        {/* <Checkbox onChange={this.onChange}>Checkbox</Checkbox> */}
                        {/* <CheckboxGroup options={plainOptions} value={this.state.checkedList} onChange={this.onChange}> */}
                          {/* <Row>
                            <Col span={24}><Checkbox value={item.id}></Checkbox></Col>
                          </Row>   */}
                        {/* </CheckboxGroup>                       */}
                        {item.name}
                    </List.Item>
                    
                  )}
                >
                  {this.state.loading && this.state.hasMore && <Spin className="demo-loading" />}
                </List>
              </InfiniteScroll>
            </div>
          </Card>
        }
      </div>
    );
  }
}
