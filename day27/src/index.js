import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './component/App';
// import ChatBox from './component/ChatBox';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(<ChatBox />, document.getElementById('root'));
registerServiceWorker();
