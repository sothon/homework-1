import React from 'react';
import Display from './Display';
import ButtonPanel from './ButtonPanel';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total: null,
      next: null,
      operation: null,
    };
  }

  handlerClick = (buttonName) => {
    console.log('buttonName=> ',buttonName)
    switch (buttonName) {
      case '1' :
      case '2' :
      case '3' :
      case '4' :
      case '5' :  
      case '6' :
      case '7' :
      case '8' :
      case '9' :
      case '0' :
      case '.' :
        if (this.state.total === 'Not a number') {
          this.setState({total: buttonName.toString()})
        } else {
          if (this.state.operation) {
            //this.setState({next: parseFloat(((this.state.next) ? this.state.next : '') + buttonName).toString()})
            this.setState({next: (((this.state.next) ? this.state.next : '') + buttonName).toString()})
          } else {
            //this.setState({total: parseFloat(((this.state.total) ? this.state.total : '') + buttonName).toString()})
            this.setState({total: (((this.state.total) ? this.state.total : '') + buttonName).toString()})
          }
        }
        break
      case 'AC' :
        this.setState({
          total: null,
          next: null,
          operation: null,
        })
        break
      case '+/-' :       
        if ((this.state.total) || (this.state.next)) {
          if (this.state.operation) {
            let minusChar = this.state.total.search('-')
            if (minusChar >= 0) {
              let result = this.state.next.replace('-', '')
              this.setState({next: result.toString()})
            } else {
              let result = '-' + this.state.next
              this.setState({next: result.toString()})
            }
          } else {
            let minusChar = this.state.total.search('-')
            if (minusChar >= 0) {
              let result = this.state.total.replace('-', '')
              this.setState({total: result.toString()})
            } else {
              let result = '-' + this.state.total
              this.setState({total: result.toString()})
            }
          } 
        }        
        break
      case '+' :        
      case '-' :  
      case 'x' :  
      case '÷' :  
      case '%' :  
        this.setState({operation: buttonName})
        break  
      case '=' :  
        let result = 0
        let error = ''
        if (this.state.operation === '+') {
          result = parseFloat(this.state.total) + parseFloat(this.state.next) 
        }
        if (this.state.operation === '-') {
          result = parseFloat(this.state.total) - parseFloat(this.state.next) 
        }
        if (this.state.operation === 'x') {
          result = parseFloat(this.state.total) * parseFloat(this.state.next) 
        }
        if (this.state.operation === '÷') {        
          result = parseFloat(this.state.total) / parseFloat(this.state.next) 
          if (this.state.next === '0') {
            error = 'Not a number'
          }
        }
        if (this.state.operation === '%') {
          result = parseFloat(this.state.total) % parseFloat(this.state.next) 
        }
        this.setState({
          total: (error !== '') ? error : result.toString(),
          next: null,
          operation: null,
        })
        break
      default :
        break
    }
  }

  render() {
    return (
      <div className="component-app">
        <Display value={this.state.next || this.state.total || '0'}/>
        <ButtonPanel clickHandler={this.handlerClick}/>
      </div>
    );
  }
}
export default App;
