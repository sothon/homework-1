
import React, { Component } from 'react';
import { List, Avatar, Card, Input, Button } from 'antd';
import './ChatBox.css';

export class ChatBox extends Component {

  constructor(props) {
    super(props);

    this.state = {
      inputText : '',
      listItem: [],
      isMe: false 
    }

    this.handleChangeText = this.handleChangeText.bind(this);

  }

  submitList = () => {

    let alignText = ''
    let nameText = ''
    let isMe = false

    if (!this.state.isMe) {
      alignText = 'right'
      nameText = 'Taey'
      isMe = true
    } else {
      if (this.state.isMe) {
        alignText = 'left'
        nameText = 'Anonymous'
        isMe = false
      } else {
        alignText = 'right'
        nameText = 'Taey'
        isMe = true
      }
    }

    let dataChat = {
      nameText: nameText,
      message: this.state.inputText,
      alignText: alignText
    }
    
    this.setState({
      // listItem: this.state.listItem.concat([this.state.inputText]),
      listItem: this.state.listItem.concat([dataChat]),
      inputText: '',
      isMe: isMe
    })

    console.log('state=>', this.state)
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submitList();
    }
  }

  handleChangeText = (event) => {
    this.setState({inputText: event.target.value});
  }

  render() {
      
    return (
      <Card style={{ width: 800 , backgroundColor : this.props.myColor }}>
            <h1>Chat-Box</h1>

            <div style={{ marginBottom:'10px' }}>
              <Input
                addonAfter={<Button type="primary" onClick={this.submitList}>Add</Button>}
                onChange={this.handleChangeText}
                value={this.state.inputText}
                onKeyPress={this.handleKeyPress}/>
            </div>

            <List
            itemLayout="horizontal"
            dataSource={this.state.listItem}
            renderItem={(item, index) => (    
                   
              <List.Item> 
                { 
                  index % 2 == 0 ?
                  <List.Item.Meta className="right"
                  avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>}
                  title={item.nameText}
                  description={item.message}
                  />
                  :
                  <List.Item.Meta className="left"
                  avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>}
                  title={item.nameText}
                  description={item.message}
                  />
                }            
                {/* <List.Item.Meta className={item.alignText}
                  avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>}
                  title={item.nameText}
                  description={item.message}
                  />   */}
              </List.Item>
            )}
            />
        </Card>
        
        
      )
    }
}

export default ChatBox;