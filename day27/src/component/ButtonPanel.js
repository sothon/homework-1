import Button from './Button';
import React from 'react';
import PropTypes from 'prop-types';

import './ButtonPanel.css';

class ButtonPanel extends React.Component {
  handlerClick = (buttonName) => {
    this.props.clickHandler(buttonName);
  }

  render() {
    return (
      <div className="component-button-panel">
        <div>
          <Button name="AC" clickHandler={this.handlerClick} yellow/>
          <Button name="+/-" clickHandler={this.handlerClick} />
          <Button name="%" clickHandler={this.handlerClick} />
          <Button name="÷" clickHandler={this.handlerClick} orange />
        </div>
        <div>
          <Button name="7" clickHandler={this.handlerClick} />
          <Button name="8" clickHandler={this.handlerClick} />
          <Button name="9" clickHandler={this.handlerClick} />
          <Button name="x" clickHandler={this.handlerClick} orange/>
        </div>
        <div>
          <Button name="4" clickHandler={this.handlerClick} />
          <Button name="5" clickHandler={this.handlerClick} />
          <Button name="6" clickHandler={this.handlerClick} />
          <Button name="-" clickHandler={this.handlerClick} orange/>
        </div>
        <div>
          <Button name="1" clickHandler={this.handlerClick} />
          <Button name="2" clickHandler={this.handlerClick} />
          <Button name="3" clickHandler={this.handlerClick} />
          <Button name="+" clickHandler={this.handlerClick} orange/>
        </div>
        <div>
          <Button name="0" clickHandler={this.handlerClick} wide />
          <Button name="." clickHandler={this.handlerClick} />
          <Button name="=" clickHandler={this.handlerClick} orange />
        </div>
      </div>
    );
  }
}
ButtonPanel.propTypes = {
  clickHandler: PropTypes.func,
};
export default ButtonPanel;