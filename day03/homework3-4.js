let fs = require('fs');

function readDatabase() {
    return new Promise(function(resolve, reject) {
        fs.readFile('homework1-4.json', 'utf8', function(err, data) {
            if (err) 
                reject(err);
            else 
                resolve(data);
        });
    });
}


async function start() {
    
    try {
        
        let data = await readDatabase();
        let jsonData = JSON.parse(data);
        let chooseArray = ['name','gender','company','email','friends'];
        let output = [];  
        jsonData.forEach(row => {
            
            let newData = {};
            // let keys = Object.keys(row)
            
            // keys.forEach(key => {            
            //     if ((key == 'name') || (key == 'gender') || (key == 'company')  || (key == 'email') || (key == 'friends')) {
            //         newData[key] = row[key];
            //     }
            // });

            chooseArray.forEach(key => {
                newData[key] = row[key];
            });

            output.push(newData);
            newData = {};
            
        });
        console.log(output);
        
    } catch(error) {
        console.error(error);
    }
}

start();
