let employees =
[
    {"id":"1001",'firstname':'Luke','lastname':'Skywalker'},
    {"id":"1002",'firstname':'Tony','lastname':'Stark'},
    {"id":"1003",'firstname':'Somchai','lastname':'Jaidee'},
    {"id":"1004",'firstname':'Monkey D','lastname':'Luffee'},
];
let company = [
    {"id":"1001","company":"Walt Disney"},
    {"id":"1002","company":"Marvel"},
    {"id":"1003","company":"Love2work"},
    {"id":"1004","company":"One Piece"},
];
let salary = [
    {"id":"1001","salary":"40000"},
    {"id":"1002","salary":"1000000"},
    {"id":"1003","salary":"20000"},
    {"id":"1004","salary":"9000000"},
];
let like = [
    {"id":"1001","like":"apple"},
    {"id":"1002","like":"banana"},
    {"id":"1003","like":"orange"},
    {"id":"1004","like":"papaya"},
];
let dislike = [
    {"id":"1001","dislike":"banana"},
    {"id":"1002","dislike":"orange"},
    {"id":"1003","dislike":"papaya"},
    {"id":"1004","dislike":"apple"},
];

let fs = require('fs');

function writeDatabase(database) {
    return new Promise(function(resolve, reject) {
        database = JSON.stringify(database);
        fs.writeFile('homework3-3.json', database, 'utf8', function(err) {
            if (err) 
                reject(err);
            else 
                resolve();
        });
    });
}
function joinDatabase() {
    let employeesDatabase = [];
    let employee = {};

    for (let index in employees) {
        for (let key in employees[index]) {
            employee[key] = employees[index][key];
        }
        for (let key in company[index]) {
            employee[key] = company[index][key];
        }
        for (let key in salary[index]) {
            employee[key] = salary[index][key];
        }
        for (let key in like[index]) {    
            employee[key] = like[index][key];
        } 
        for (let key in dislike[index]) {        
            employee[key] = dislike[index][key];
        } 
        employeesDatabase.push(employee);
        employee = {};
    }
    return employeesDatabase;
}

writeDatabase(joinDatabase());

