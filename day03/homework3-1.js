let fs = require('fs');

function readData(filename) {
    return new Promise(function(resolve, reject) {
        fs.readFile(filename, 'utf8', function(err, data) {
            if (err) 
                reject(err);
            else             
                resolve(data);
        });
    });
}

Promise.all([
            readData('head.txt'), 
            readData('body.txt'), 
            readData('leg.txt'), 
            readData('head.txt')
        ])
.then(function(result) {

    let robot = result.join('\n');

    let writeRobot = new Promise(function(resolve, reject) {
        fs.writeFile('robot.txt', robot, 'utf8', function(err) {
            if (err) 
                reject(err);
            else 
                resolve();
        });    
    });

})
.catch(function(error) {
    console.error("There's an error", error); 
});