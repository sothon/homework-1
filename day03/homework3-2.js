let fs = require('fs');

function writeData(dataRobot) {
    return new Promise(function(resolve, reject) {
        fs.writeFile('robot.txt', dataRobot, 'utf8', function(err) {
            if (err) 
                reject(err);
            else 
                resolve();
        });
    });
}

function readData(filename) {
    return new Promise(function(resolve, reject) {
        fs.readFile(filename, 'utf8', function(err, data) {
            if (err) 
                reject(err);
            else             
                resolve(data);
        });
    });
}

async function robot() {
    let result = [];
    try {

        result.push(await readData('head.txt'));
        result.push(await readData('body.txt'));
        result.push(await readData('leg.txt'));
        result.push(await readData('feet.txt'));

        let robot = result.join("\n");
        await writeData(robot);

        
    } catch(error) {
        console.error(error);
    }
}

robot();