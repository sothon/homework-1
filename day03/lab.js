let fs = require('fs');

//##### 1 #####
// fs.readFile('head.txt', 'utf8', function(err, data) {
    
//     if (err) 
//         console.error(err);
//     else
//         console.log(dataDemo1);

// });

//##### 2 #####
function writeDemo1() {
    return new Promise(function(resolve, reject) {
        fs.writeFile('demofile1.txt', 'test', 'utf8', function(err) {
            if (err) 
                reject(err);
            else
                resolve();
        });
    });
}

function readDemo1() {
    return new Promise(function(resolve, reject) {
        fs.readFile('demofile1.txt', 'utf8', function(err, dataDemo1) {
            if (err) 
                reject(err);
            else 
                resolve(dataDemo1);
        });
    });
}

function writeDemo2(dataDemo1) {
    return new Promise(function(resolve, reject) {
        fs.writeFile('demofile2.txt', dataDemo1, 'utf8', function(err) {
            if (err)
                reject(err);
            else 
                resolve("Promise Success!");
        });
    });
}

// writeDemo1().then(function() {
//     return readDemo1();
// }).then(function(dataDemo1) {
//     return writeDemo2(dataDemo1);
// }).then(function(data) { 
//     console.log(data);
// }).catch(function(error) {
//     console.error(error)
// });

//##### 3 #####
// async function copyFile() {
//     try {
//         await writeDemo1();
//         let dataDemo1 = await readDemo1();
//         await writeDemo2(dataDemo1);
//         console.log('Asyns/Await Success!');
//     } catch(error) {
//         console.error(error);
//     }
// }
// copyFile();

//##### 4 #####
let p1 = new Promise(function(resolve, reject) {
    resolve('one');
});
let p2 = new Promise(function(resolve, reject) {
    resolve('two');
});
let p3 = new Promise(function(resolve, reject) {
    resolve('three');
});
Promise.all([p1, p2, p3])
.then(function(result) {
    console.log('All completed!: ', result); // result = ['one','two','three']
})
.catch(function(error) {
    console.error("There's an error", error); 
});

