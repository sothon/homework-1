class Users {
    constructor() {
        
    }
    async getUserAll() {

        const { db } = require('../lib/db.js')
        const connection = await db.connection;

        const [rows, fields] = await db.exec(connection, 'SELECT * FROM user');

        return rows;
    }
}

exports.Users = new Users();