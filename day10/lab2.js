// const mysql = require('mysql2');
// const connection = mysql.createConnection({
//     host: 'localhost', 
//     user: 'root', 
//     password: 'root',
//     database: 'codecamp'
// });
// connection.query('SELECT * FROM user',
//     function(err, results, fields) {
//         console.log(results); // results contains rows returned by server
//         console.log(fields); // fields contains extra meta data about results, if available
//     }
// );

async function start() {
    // get the client
    const mysql = require('mysql2/promise');
    // create the connection
    const connection = await mysql.createConnection({
        host:'localhost',
        // host:'127.0.0.1',
        user: 'root',
        password: 'root',
        database: 'codecamp'
    });
    // query database
    const [rows, fields] = await connection.execute('SELECT * FROM user');

    console.log(rows);
}

start();
