
const Koa = require('koa');
const app = new Koa();

const render = require('koa-ejs');
const path = require('path'); 

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});

const otherMiddleware = require('./controller/routes.js')(app);

app.listen(3000);