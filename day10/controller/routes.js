
module.exports = function(app) { app.use(async (ctx, next) => {
    try {
        
        const { Users } = require('../models/homework10_1.js');
        const rows = await Users.getUserAll();
        
        await ctx.render('homework10-1', { "users":rows });
        await next();
    } catch (err) {
        ctx.status = 400
        // ctx.body = 'Uh-oh:${err.message}'
        ctx.body = err.message
    }
  });
}