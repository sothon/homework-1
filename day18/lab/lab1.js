const Koa = require('koa')
const multer = require('koa-multer')
const sharp = require('sharp')
const fs = require('fs')

const upload = multer({ dest: 'upload/' })

const app = new Koa()

app.use(async (ctx) => {

    await upload.single('file')(ctx)
    const tempFile = ctx.req.file.path
    const outFile = tempFile + '.png'
    await sharp(tempFile)
        .resize(100, 100)
        .toFile(outFile)

    fs.unlink(tempFile, () => {})
    ctx.body = outFile

})

app.listen(3000)