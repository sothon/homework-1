let fs = require('fs');

function writeData(filename, data) {
    return new Promise(function(resolve, reject) {
        let writeString = JSON.stringify(data);
        if (writeString.length > 0) {
            fs.writeFile(filename, writeString, 'utf8', function(err) {
                if (err) 
                    reject(err);
                else             
                    resolve();
            });
        } else {
            reject('err:empty string to write file');
        }
    });
}

exports.writeData = writeData;

