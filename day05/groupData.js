let fs = require('fs');
let writeData = require('./writeData');

function groupData(data, condition) {
    return new Promise(function(resolve, reject) {
        group = {};
        if (condition === '') {
            reject('err');
        } else {
            let result = data.map(element => {
                Object.keys(element).map(key => {
                    if (condition === key) {                
                        if (group.hasOwnProperty(element[key])) {
                            group[element[key]]++;
                        } else {
                            group[element[key]] = 1;
                        }
                    }            
                });    
            });

            if (condition == 'eyeColor') { condition = 'eyes'; }
            writeData.writeData('./homework5-1_'+condition+'.json', group);

            resolve(group);       
        }
    });
}

exports.groupData = groupData;
