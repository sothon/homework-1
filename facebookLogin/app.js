const Koa = require('koa');
const render = require('koa-ejs');
const path = require('path');

const app = new Koa();
 
// response
render(app, {
    root: path.join(__dirname, 'view'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
  });
   
  app.use(async function (ctx) {
    await ctx.render('index');
  });
 
app.listen(3000);