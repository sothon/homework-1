จากการบานท 1 ขอ 2 นกเรยนแตละคน ซอคอรสไปคนละเทาไร จนวนกคอรส (ใหใชแคstatement เดยวเทานน)
select 
    students.id, students.name, sum(courses.price) as total_amount, count(courses.price) as total_course
from courses
    inner join enrolls on courses.id = enrolls.course_id
    inner join students on enrolls.student_id = students.id
group by students.id, students.name;


นกเรยนแตละคน ซอคอรสไหน ราคาแพงสด
select 
    distinct students.id, students.name, courses.id as courses_id, courses.price
    , max.max_price
from courses
    inner join enrolls on courses.id = enrolls.course_id
    inner join students on enrolls.student_id = students.id
    inner join (
        select 
            students.id, students.name, max(courses.price) as max_price
        from courses
            inner join enrolls on courses.id = enrolls.course_id
            inner join students on enrolls.student_id = students.id
        group by students.id, students.name
    ) as max on max.id = students.id
where 
    courses.price = max.max_price;

