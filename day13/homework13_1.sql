
จากคอรสทมคนเรยนทงหมด ไดเงนเทาไร
select 
    sum(courses.price) as total_amount
from courses
    inner join enrolls on courses.id = enrolls.course_id;

นกเรยนแตละคน ซอคอรสไปคนละเทาไร
select 
    students.id, students.name, sum(courses.price) as total_amount
from courses
    inner join enrolls on courses.id = enrolls.course_id
    inner join students on enrolls.student_id = students.id
group by students.id, students.name;