
default isolation

show variables like '%isola%';
+---------------+-----------------+
| Variable_name | Value           |
+---------------+-----------------+
| tx_isolation  | REPEATABLE-READ |
+---------------+-----------------+
1 row in set (0.00 sec)


----Dirty Reads-----
set transaction isolation level read uncommitted;
begin;
update courses set name = 'Test' where id = 1
select * from courses
rollback;

----Non-Repeatable Reads----
set transaction isolation level read uncommitted;
begin;
select * from courses
update courses set name = 'Test' where id = 1
commit;
select * from courses

----Phantom Reads----
set transaction isolation level read uncommitted;

begin;
select count(*) from enrolls
insert into enrolls (student_id,course_id) values (1,3)
commit;
select count(*) from enrolls



Level                   Dirty reads     Non-repeatable reads    Phantoms reads
READ UNCOMMITTED        Yes             Yes                     Yes
READ COMMITTED          No              Yes                     Yes
REPEATABLE READ         No              No                      No  
SERIALIZABLE            No              No                      No  


