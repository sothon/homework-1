หารายไดเฉลยของแตละตแหนง

ตแหนงไหนไดรายได มากสด, นอยสด

แตละตแหนงมพนกงานกคน


select 
    titles.title, sum(salaries.salary), count(salaries.salary), avg(salaries.salary), max(salaries.salary), min(salaries.salary)
from     
    employees
    inner join salaries on salaries.emp_no = employees.emp_no    
    inner join titles on titles.emp_no = employees.emp_no    
where
    salaries.to_date >= now()   
    and titles.to_date >= now()   
group by  titles.title  
order by  titles.title  