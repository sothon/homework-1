let arr= [1,2,3,4,5,6,7,8,9,10];

const result = arr.filter(member => {
    return member % 2 === 0 ? member : null;
})
.map(member => {
    return member * 1e3;
});

console.log(result);