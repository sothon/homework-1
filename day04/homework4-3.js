
let fs = require('fs');

function readFile() {
    return new Promise(function(resolve, reject) {
        fs.readFile('homework1.json', 'utf8', function(err, data) {
            if (err) 
                reject(err);
            else 
                resolve(data);
        });
    });
}

async function start() {
    let data = await readFile();
    let jsonData = JSON.parse(data); 

    const filterSalary = jsonData.filter(employee => {
        if (employee['salary'] < 1e5) { 
            employee['salary'] = employee['salary'] * 2;
            return employee; 
        } 
        //else { 
            //return false; 
        //}
    }); 

    let summarySalaryLess = filterSalary.reduce((sum, employee) => {
        return sum + employee['salary'];
    }, 0);

    let summarySalaryMore = jsonData.filter(employee => {
        if (employee['salary'] >= 1e5) {         
            return employee; 
        } else { 
            return false; 
        }
    }).reduce((sum, employee) => {
        return sum + employee['salary'];
    }, 0);
    
    console.log(filterSalary);
    console.log('Summary of Salary = ', summarySalaryLess + summarySalaryMore);

}

start();