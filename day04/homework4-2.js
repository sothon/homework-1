
let fs = require('fs');

function readFile() {
    return new Promise(function(resolve, reject) {
        fs.readFile('homework1.json', 'utf8', function(err, data) {
            if (err) 
                reject(err);
            else 
                resolve(data);
        });
    });
}

function addNextSalary(currentSalary) {
    let next3YearSalary = [0, 0, 0];
    next3YearSalary.map((salary, index) => {
        currentSalary += (currentSalary * (10/100));
        next3YearSalary[index] = currentSalary;
    });
    return next3YearSalary;
};

async function start() {
    let data = await readFile();
    let jsonData = JSON.parse(data); 
    
    const employees = [...jsonData]; 
    
    employees.map(employee => {

        employee['fullname'] = employee['firstname'] + ' ' + employee['lastname'];
        employee['salary'] = addNextSalary(employee['salary']);

    });

    console.log(employees); 

}

start();