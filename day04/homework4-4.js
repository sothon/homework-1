
let fs = require('fs');

function readFile() {
    return new Promise(function(resolve, reject) {
        fs.readFile('homework1-4.json', 'utf8', function(err, data) {
            if (err) 
                reject(err);
            else 
                resolve(data);
        });
    });
}

async function start() {
    let data = await readFile();
    let jsonData = JSON.parse(data); 

    const employees = jsonData
    .filter(employee => {

        if ((employee['friends'].length >= 2) && (employee['gender'] === 'male')) { 

            let keys = Object.keys(employee);
            keys.map(key => {
                if ((key !== 'name') && (key !== 'gender') && (key !== 'company') && (key !== 'email') && (key !== 'friends') && (key !== 'balance')) {
                    delete employee[key];
                } 
            });
           
            let newBalance = '$' + employee['balance'].slice(1).replace(/,/,'') / 10;
            employee['balance'] = newBalance;            
            return employee; 
        } else {
            return false; 
        }
        
    });
    console.log(employees);
}

start();

