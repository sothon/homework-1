let assert = require('assert');
let sinon = require('sinon');
let fs = require('fs');
let lab6 = require('./lab6_1.js');

/************ SPY *************/
it('should write JSON string into output6_1.txt. check with spy', function() { 
    const save = sinon.spy(fs, 'writeFile');
    const user = {
        "firstname" : "Somchai",
        "lastname" : "Sudlor" 
    };
    let dummyCallbackFunction = function(err) {}; 
    lab6.saveUserDatabase(user, "output6_1.txt", dummyCallbackFunction);
    console.log(save.callCount);
    save.restore(); 
    sinon.assert.calledOnce(save); 
    sinon.assert.calledWith(save,'output6_1.txt');
});


/************ STUB *************/
it('should write JSON string into output6_1.txt check with stub', function() {
    const save = sinon.stub(fs, 'writeFile');
    save.yields(null);
    const callbackFunction = sinon.spy();
    const user = {
      "firstname" : "Somchai",
      "lastname" : "Sudlor"
    };
    lab6.saveUserDatabase(user, "output6_1.txt", callbackFunction);
    
    save.restore();
    sinon.assert.calledOnce(save); 
    sinon.assert.calledWith(callbackFunction, null);
});

/************ MOCK *************/
it('should write JSON string into output6_1.txt. check with mock', function() { 
    const user = {
        "firstname" : "Somchai",
        "lastname" : "Sudlor"
    };
    let mockFs = sinon.mock(fs); 
    mockFs.expects('writeFile').once().withArgs("output6_1.txt");
    lab6.saveUserDatabase(user, "output6_1.txt", function(err) {} );
    mockFs.verify();
    mockFs.restore();
});