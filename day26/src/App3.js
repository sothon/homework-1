import React, { Component } from 'react';   
import {Form,Button,Icon,Input, Row, Col} from 'antd';
import './App.css';
const FormItem = Form.Item;

class App extends Component {

  render() {
    
    return (

    <div>
    <Row style={{ textAlign:'center'}} >
      <Col md={12} sm={16} className="changeBG" >
      <Row >
        <img src={'./half-left.jpg'} alt="left-desc" style={{width:'100%', height:'100%'}} />
      </Row>
      </Col>
      
      <Col style={{paddingTop:"25px"}} span={12} >
        <Form layout="inline" onSubmit={this.handleSubmit}>
            <FormItem>
            <Input style={{ color: 'rgba(0,0,0,.25)' }}  placeholder="Username" />
            </FormItem>
            <FormItem>
            <Input  style={{ color: 'rgba(0,0,0,.25)' }} type="password" placeholder="Password" />
            <a href="" style={{ color: 'rgba(0,0,0,.25)' }}>ลืมรหัสผ่าน?</a>
            </FormItem>
            <FormItem>
            <Button type="primary"htmlType="submit">Log in</Button>
            </FormItem>
        </Form>
        <Form className="login-form" style={{ width:"250px" ,textAlign:"left", margin: 'auto', marginTop: '150px' }}>
            <Row>
            <Col span={12} style={{textAlign:"left"}} >
                <img width={30} alt="logo" src="https://upload.wikimedia.org/wikipedia/th/thumb/6/62/Twitter_bird_logo_2012.png/150px-Twitter_bird_logo_2012.png" />
            </Col>
                <Col span={12} style={{textAlign:"right"}}>
            </Col>
            </Row>
            <h2 className="StaticLoggedOutHomePage-signupTitle">ดูสิ่งที่เกิดขึ้นบนโลกในขณะนี้</h2>
            <h3 className="StaticLoggedOutHomePage-signupSubtitle">เข้าร่วมทวิตเตอร์วันนี้</h3>
            <Form.Item>
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}placeholder="Username" />
            </Form.Item>
            <Form.Item>
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}type="password" placeholder="Password" />
            </Form.Item>
            <Form.Item>
            <Row>
                <Col span={12} style={{textAlign:"left"}} >
                <Button type="primary" htmlType="submit">Sign in</Button>
                </Col>
                <Col span={12} style={{textAlign:"right"}} >มีบัญชีผู้ใช้อยู่แล้ว? <a href="/login">เข้าสู่ระบบ</a>
                </Col>
            </Row>
            </Form.Item>
        </Form>
      </Col>
    </Row>
    <Row style={{ textAlign:'center', fontSize: '10px', marginTop: '10px'}} >
        <div class="StreamsFooter StreamsFooter--fixed">
            <ul class="StreamsFooter-list u-cf">
                <li class="StreamsFooter-item"><a href="/about" rel="noopener">เกี่ยวกับ</a></li>
                <li class="StreamsFooter-item"><a href="//support.twitter.com" rel="noopener">ศูนย์ช่วยเหลือ</a></li>
                <li class="StreamsFooter-item"><a href="https://blog.twitter.com" rel="noopener">บล็อก</a></li>
                <li class="StreamsFooter-item"><a href="http://status.twitter.com" rel="noopener">สถานะ</a></li>
                <li class="StreamsFooter-item"><a href="https://about.twitter.com/careers" rel="noopener">ตําแหน่งงาน</a></li>
                <li class="StreamsFooter-item"><a href="/tos" rel="noopener">ข้อตกลง</a></li>
                <li class="StreamsFooter-item"><a href="/privacy" rel="noopener">นโยบายเกี่ยวกับความเป็นส่วนตัว</a></li>
                <li class="StreamsFooter-item"><a href="//support.twitter.com/articles/20170514" rel="noopener">คุกกี้</a></li>
                <li class="StreamsFooter-item"><a href="//support.twitter.com/articles/20170451" rel="noopener">ข้อมูลโฆษณา</a></li>
                <li class="StreamsFooter-item"><a href="//about.twitter.com/press/brand-assets" rel="noopener">ยี่ห้อ</a></li>
                <li class="StreamsFooter-item"><a href="https://about.twitter.com/products" rel="noopener">แอพ</a></li>
                <li class="StreamsFooter-item"><a href="//ads.twitter.com/?ref=gl-tw-tw-twitter-advertise" rel="noopener">ลงโฆษณา</a></li>
                <li class="StreamsFooter-item"><a href="https://marketing.twitter.com" rel="noopener">การตลาด</a></li>
                <li class="StreamsFooter-item"><a href="https://business.twitter.com" rel="noopener">ธุรกิจต่างๆ</a></li>
                <li class="StreamsFooter-item"><a href="//dev.twitter.com" rel="noopener">นักพัฒนา</a></li>
                <li class="StreamsFooter-item"><a href="/i/directory/profiles" rel="noopener">สารบบ</a></li>
                <li class="StreamsFooter-item"><a href="/settings/personalization" rel="noopener">การตั้งค่า</a></li>
                <li class="StreamsFooter-item StreamsFooter-copyright">© 2018 ทวิตเตอร์</li>
            </ul>
        </div>
    </Row>
    </div>
                );
              }
            }
export default App;